#!/bin/sh
set -e
# first arg is `-f` or `--some-option`
if [ "${1#-}" != "$1" ]; then
  set -- php-fpm "$@"
fi
#if [ "$1" = 'php-fpm' ] || [ "$1" = 'bin/console' ] || { [ "$1" = 'php' ] && [ "$2" = 'bin/console' ]; }; then
#  mkdir -p var/cache var/log config/jwt
#  chown -R www-data:www-data var
#  jwt_passphrase=$(grep '^JWT_PASSPHRASE=' .env | cut -f 2 -d '=')
#  if [ ! -f config/jwt/private.pem ]; then
#    echo "Generating private key for JWT"
#    echo "$jwt_passphrase" | openssl genpkey -out config/jwt/private.pem -pass stdin -aes256 -algorithm rsa -pkeyopt rsa_keygen_bits:4096
#  fi
#  if [ ! -f config/jwt/public.pem ]; then
#    echo "Generating public key for JWT"
#    echo "$jwt_passphrase" | openssl pkey -in config/jwt/private.pem -passin stdin -out config/jwt/public.pem -pubout
#  fi

#  echo "Waiting for db to be ready..."
#  until bin/console doctrine:query:sql "SELECT 1" >/dev/null 2>&1; do
#    sleep 1
#  done
#
#  if [ "$APP_ENV" != 'prod' ]; then
#    echo "Executing DB migrations"
#    bin/console doctrine:migrations:migrate --no-interaction >/dev/null
#  fi
#fi

echo "Setup done."
exec docker-php-entrypoint "$@"
