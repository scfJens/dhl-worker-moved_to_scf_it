# the different stages of this Dockerfile are meant to be built into separate images
# https://docs.docker.com/compose/compose-file/#target

ARG PHP_VERSION=7.4
ARG NGINX_VERSION=1.15
ARG VARNISH_VERSION=6.0
ARG ALPINE_VERSION=3.10

FROM php:${PHP_VERSION}-fpm-alpine${ALPINE_VERSION} AS api_platform_php

#RUN docker-php-ext-install mysqli pdo_mysql

# persistent / runtime deps
RUN apk add --no-cache \
	acl \
	file \
	gettext \
	git \
	postgresql-client \
	;

ARG APCU_VERSION=5.1.20
RUN set -eux; \
	apk add --no-cache --virtual .build-deps \
	$PHPIZE_DEPS \
	icu-dev \
	libzip-dev \
	postgresql-dev \
	zlib-dev \
	; \
	\
	docker-php-ext-configure zip; \
	docker-php-ext-install -j$(nproc) \
	mysqli \
	pdo_mysql \
	intl \
	pdo_pgsql \
	zip \
	; \
	pecl install \
	apcu-${APCU_VERSION} \
	; \
	pecl clear-cache; \
	docker-php-ext-enable \
	apcu \
	opcache \
	; \
	\
	runDeps="$( \
	scanelf --needed --nobanner --format '%n#p' --recursive /usr/local/lib/php/extensions \
	| tr ',' '\n' \
	| sort -u \
	| awk 'system("[ -e /usr/local/lib/" $1 " ]") == 0 { next } { print "so:" $1 }' \
	)"; \
	apk add --no-cache --virtual .api-phpexts-rundeps $runDeps; \
	\
	apk del .build-deps

#COPY php_mysql_xdevapi.dll $PHP_INI_DIR/ext/
COPY --from=composer:2.2 /usr/bin/composer /usr/bin/composer
RUN ln -s $PHP_INI_DIR/php.ini-production $PHP_INI_DIR/php.ini
COPY docker/php/conf.d/api-platform.ini $PHP_INI_DIR/conf.d/api-platform.ini

# https://getcomposer.org/doc/03-cli.md#composer-allow-superuser
ENV COMPOSER_ALLOW_SUPERUSER=1
# install Symfony Flex globally to speed up download of Composer packages (parallelized prefetching)
RUN composer global config --no-plugins allow-plugins.symfony/flex true \
	&& composer global require "symfony/flex" --prefer-dist --no-progress --no-suggest --classmap-authoritative
ENV PATH="${PATH}:/root/.composer/vendor/bin"

WORKDIR /srv/api
# build for production
ARG APP_ENV=prod

ARG MAILER_URL=null://localhost

# prevent the reinstallation of vendors at every changes in the source code
COPY composer.json composer.lock symfony.lock ./
# do not use .env files in production
RUN echo '<?php return [];' > .env.local.php
RUN set -eux; \
	composer install --prefer-dist --no-dev --no-autoloader --no-scripts --no-progress --no-suggest; \
	composer clear-cache

# copy only specifically what we need
COPY bin bin/
COPY config config/
COPY public public/
COPY src src/
COPY assets assets/
COPY migrations migrations/
#COPY templates templates/

RUN set -eux; \
	mkdir -p var/cache var/log; \
	composer config --no-plugins allow-plugins.symfony/flex true; \
	composer dump-autoload --classmap-authoritative --no-dev; \
	composer run-script --no-dev post-install-cmd; \
	chmod +x bin/console; sync
VOLUME /srv/api/var

COPY docker/php/entrypoint.sh /usr/local/bin/docker-entrypoint
RUN chmod +x /usr/local/bin/docker-entrypoint
ENTRYPOINT ["docker-entrypoint"]

CMD ["php-fpm"]

#FROM nginx:${NGINX_VERSION}-alpine AS api_platform_nginx
#
#COPY docker/nginx/conf.d/default.conf /etc/nginx/conf.d/default.conf

#COPY --from=api_platform_php /srv/api/public public/

#FROM cooptilleuls/varnish:${VARNISH_VERSION}-alpine AS api_platform_varnish
#
#COPY docker/varnish/conf/default.vcl /usr/local/etc/varnish/default.vcl
