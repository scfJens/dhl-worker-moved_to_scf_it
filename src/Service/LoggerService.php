<?php
/**
 * Created by PhpStorm.
 * User: Jens
 * Date: 14.02.2019
 * Time: 13:51
 */

namespace App\Service;

use Psr\Log\LoggerInterface;
use Symfony\Bridge\Monolog\Logger;


// by always using this class when logging, we stay flexible, and can easily modify the logging concept in one central place

class LoggerService
{
    /**
     * @var Logger
     */
    private $logger; // the monolog logger is configured in config/packages/dev/monolog.yaml and config/packages/prod/monolog.yaml


    public function __construct(LoggerInterface $loggerIf)
    {
        $this->logger = $loggerIf;
    }

    function logEmgcy(string $logTxt, array $context = array())
    {
        $this->logger->emergency(getenv('SLACK_LOG_PREFIX') . $logTxt, $context);
        die($logTxt); // docker will restart all processes incl. the python repeater
    }

    function logDebug(string $logTxt, array $context = array())
    {
        if (getenv('LOGGING_ON_PHPSTORM'))
            echo($logTxt);
        else
            $this->logger->debug($logTxt, $context);
    }

    function logError(string $logTxt, array $context = array())
    {
        $this->logger->error(getenv('SLACK_LOG_PREFIX') . $logTxt, $context);
        if (getenv('LOGGING_ON_PHPSTORM'))
            echo($logTxt);
    }

    function logNotice(string $logTxt, array $context = array())
    {
        $this->logger->notice($logTxt, $context);
        if (getenv('LOGGING_ON_PHPSTORM'))
            echo($logTxt);
    }

    function logInfo(string $logTxt, array $context = array())
    {
        $this->logger->info($logTxt, $context);
        if (getenv('LOGGING_ON_PHPSTORM'))
            echo($logTxt);
    }


}
