<?php

namespace App\Service;

use App\Model\SqsMessage;
use Aws\Credentials\Credentials;
use Aws\Exception\AwsException;
use Aws\Result;
use Aws\S3\S3Client;
use Aws\Sqs\SqsClient;
use Exception;


/* // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* // SqsService: used for receiving requests and sending responses to TMS. Also used for sending requests and receiving responses from BirtWorker */
/* // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

class SqsService
{
    const MAX_RETRIES = 5;

    /** @var S3Client */
    private $s3Client;

    /** @var SqsClient */
    private $sqsClient;

    /** @var SqsClientExtended */
    private $sqsExtendedClient;

    /** @var String */
    private $birtRequestQueueUrl, $birtResponseQueueUrl; // queues to/from BirtWorker

    /** @var String */
    private $tpoRequestQueueUrl, $tpoResponseQueueUrl; // queues from/to TMS

    /** @var LoggerService */
    private $loggerService;

    public function __construct(LoggerService $logger, SqsClientExtended $sqsExt)
    {
        $this->loggerService = $logger;
        $this->sqsExtendedClient = $sqsExt;
        $this->initAWS();
        try {
            $this->initQueues();
//            $this->loggerService->logDebug("sqs client just initialized: $this->birtRequestQueueUrl, $this->birtResponseQueueUrl, $this->tpoRequestQueueUrl and $this->tpoResponseQueueUrl");
        } catch (Exception $e) {
            $errMsg = "SQS Queue Initialization failed, now terminating: " . $e->getMessage();
            $this->loggerService->logError($errMsg);
            die($errMsg);
        }
    }

    function closeTransport()
    {
//        todo:
    }

    /**
     * Will return normally, even if the credentials are false, but subsequent operations will throw exceptions
     */
    function initAWS()
    {
        if (!isset($_SERVER['AWS_ACCESS_KEY_ID']) || !isset($_SERVER['AWS_SECRET_ACCESS_KEY']) || !isset($_SERVER['AWS_REGION'])
        || !isset($_SERVER['DHL_REQUEST_QUEUE']) || !isset($_SERVER['DHL_RESPONSE_QUEUE'])
        || !isset($_SERVER['BIRT_REQUEST_QUEUE']) || !isset($_SERVER['BIRT_RESPONSE_QUEUE'])) {
            $this->loggerService->logError("Missing credentials for AWS SQS");
            $this->sqs = null;
            return;
        }
        $credentials = new Credentials($_SERVER['AWS_ACCESS_KEY_ID'], $_SERVER['AWS_SECRET_ACCESS_KEY']);
// initialize the S3 client
        $this->s3Client = new S3Client([
            'version' => 'latest',
            'region' => $_SERVER['AWS_REGION'],
            'credentials' => $credentials
        ]);
// initialize the SQS client
        $this->sqsClient = new SqsClient([
            'version' => 'latest',
            'region' => $_SERVER['AWS_REGION'],
            'credentials' => $credentials
        ]);
        // initialize the SQS-Extended client
        $s3BucketName = $_SERVER['S3BUCKET_SQS_EXTENDED'];
        $this->sqsExtendedClient->initSqsExtendedClient($this->sqsClient, $this->s3Client, $s3BucketName, SqsClientExtended::IF_NEEDED);
    }

    /**
     * inits all 4 queues.
     * The Req/Resp queues from/to TMS are created if not already existing
     * The Req/Resp queues to/from the BirtWorker are under responsibility of the BirtWorker, so we throw an error if they do not exist
     * @throws Exception
     */
    function initQueues()
    {
        // initialize Birt RequestQueue. If not existing, throw Error
        $reqQueueName = $_SERVER['BIRT_REQUEST_QUEUE'];
        try {
            $this->birtRequestQueueUrl = $this->sqsClient->getQueueUrl(['QueueName' => $reqQueueName])->get("QueueUrl");
//            $this->loggerService->logDebug(sprintf("Birt RequestQueue Url: %s", $this->birtRequestQueueUrl));
        } catch (Exception $e) {
            $this->loggerService->logDebug(sprintf("EXCEPTION: %s", $e));
            throw new Exception("BIRT Request Queue not found: " . $reqQueueName . " BIRT Worker is responsible for creation");
        }
        // initialize Birt ResponseQueue. If not existing, throw Error
        $respQueueName = $_SERVER['BIRT_RESPONSE_QUEUE'];
        try {
            $this->birtResponseQueueUrl = $this->sqsClient->getQueueUrl(['QueueName' => $respQueueName])->get("QueueUrl");
//            $this->loggerService->logDebug(sprintf("Birt ResponseQueue Url: %s", $this->birtResponseQueueUrl));
        } catch (Exception $e) {
            $this->loggerService->logDebug(sprintf("EXCEPTION: %s", $e));
            throw new Exception("BIRT Response Queue not found: " . $respQueueName . " BIRT Worker is responsible for creation");
        }

        // create EDI RequestQueue if needed
        $reqQueueName = $_SERVER['DHL_REQUEST_QUEUE'];
        try {
            $this->tpoRequestQueueUrl = $this->sqsClient->getQueueUrl([
                'QueueName' => $reqQueueName
            ])->get("QueueUrl");
//            $this->loggerService->logDebug(sprintf("EDI RequestQueue Url: %s", $this->ediRequestQueueUrl));
        } catch (Exception $e) {
            $this->loggerService->logInfo($reqQueueName . " not existing, now creating");
            try {
                $result = $this->sqsClient->createQueue([
                    'QueueName' => $reqQueueName
                ]);
                $this->tpoRequestQueueUrl = $result->get("QueueUrl");
                $this->loggerService->logDebug(sprintf("EDI RequestQueue Url (just created): %s", $this->tpoRequestQueueUrl));
            } catch (AwsException $e) {
                throw new Exception(sprintf("EDI RequestQueue creation FAILED: %s", $e->getMessage()));
            }
        }
        // create EDI ResponseQueue if needed
        $respQueueName = $_SERVER['DHL_RESPONSE_QUEUE'];

        try {
            $this->tpoResponseQueueUrl = $this->sqsClient->getQueueUrl([
                'QueueName' => $respQueueName
            ])->get("QueueUrl");
//            $this->loggerService->logDebug(sprintf("EDI ResponseQueue Url: %s", $this->ediResponseQueueUrl));
        } catch (Exception $e) {
            $this->loggerService->logInfo($respQueueName . " not existing, now creating");
            try {
                $result = $this->sqsClient->createQueue([
                    'QueueName' => $respQueueName,
                    'Attributes' => [
                        'MessageRetentionPeriod' => 3600,
                        'VisibilityTimeout' => 30
                    ],
                ]);
                $this->tpoResponseQueueUrl = $result->get("QueueUrl");
                $this->loggerService->logDebug(sprintf("EDI ResponseQueue (just created): %s", $this->tpoResponseQueueUrl));
            } catch (AwsException $e) {
                throw new Exception(sprintf("EDI ResponseQueue creation FAILED: %s", $e->getMessage()));
            }
        }
    }

    /**
     * used for getting a pdf from the BirtWorker
     * @param string $msgBody
     * @param array $extendedPayloadAttr // in case we are forwarding an already existing S§ pointer, the attr needs to be copied also to the fwd msg
     * @return string|null
     */
    function doRpcCall(string $msgBody, array $extendedPayloadAttr = null): ?string
    {
        if (!$this->sqsClient) {
            $this->loggerService->logError("Missing credentials for AWS SQS");
            return null;
        }

        try {
            $result = $this->sqsExtendedClient->sendLargeMessage($msgBody, $this->birtRequestQueueUrl, null, $extendedPayloadAttr);
//        var_dump($result);
            $sentId = $result->get("MessageId");
            $this->loggerService->logDebug("SQS Msg sent, id: " . $sentId);
        } catch (AwsException $e) {
            // output error message if fails
            $this->loggerService->logError("ERROR: Failed sending SQS message: " . $e->getMessage());
            return null;
        }

        /* @var $result Result */
        $result = null;
        $retryCounter = 0;
        do {
            try {
                $result = $this->sqsClient->receiveMessage([
                    'QueueUrl' => $this->birtResponseQueueUrl,
                    'WaitTimeSeconds' => 10,
                    'MaxNumberOfMessages' => 5,
                    "MessageAttributeNames" => ["correlationId"]
                ]);
            } catch (Exception $e) {
                $this->loggerService->logError("ERROR: Failed receiving SQS responses: " . $e->getMessage());
                return null;
            }
            $msgCount = empty($result->get('Messages')) ? 0 : count($result->get("Messages"));
            $this->loggerService->logDebug("received " . $msgCount . " message" . ($msgCount !== 1 ? "s" : ""));
            if ($msgCount)
                foreach ($result->get("Messages") as $msg) {
                    /* @var $message SqsMessage */
                    try {
                        $message = new SqsMessage($msg); // initiate helperObject, converting the anonymous array to an object
                        /* @var $msgAttrs [] */
                        $msgAttrs = $message->getMessageAttributes();
//                        var_dump($msgAttrs);
                        $correlationId = (array_key_exists("correlationId", $msgAttrs) && $msgAttrs["correlationId"]["DataType"] === "String") ? $msgAttrs["correlationId"]["StringValue"] : "nix";
                        if ($correlationId === $sentId) {
                            $this->loggerService->logDebug("got reply");
                            $response = $message;
                        } else {
                            $this->loggerService->logDebug(sprintf("got message, but ID not fitting. Expecting %s -> got %s", $sentId, $correlationId));
                            // if we are alone on this queue, we can delete this msg, otherwise we'll get it for every sqs reconnect until the message expires (after 1 hour) or is deleted by another receiver
                        }
                    } catch (Exception $e) {
                        $this->loggerService->logError("ediWorker: something went wrong during response reception: " . $e);
                        // Optional : Set the VisibilityTimeout to 0 to be instantaneously requeued. Doesn't make sense here, since we are alone listening to the queue
                        /*       $this->sqs->changeMessageVisibility([
                                   'QueueUrl' => $this->sqsResponseQueueUrl,
                                   'ReceiptHandle' => $message->getReceiptHandle(),
                                   'VisibilityTimeout' => 1,
                               ]);*/
                        return null;
                    }
                }
        } while (!$response && $retryCounter++ < self::MAX_RETRIES);
        // When finished, delete the message
        if ($response)
            $this->sqsClient->deleteMessage([
                'QueueUrl' => $this->birtResponseQueueUrl,
                'ReceiptHandle' => $response->getReceiptHandle(),
            ]);
        return $this->getPayload($response);
    }


    /**
     * Gets the payload, either from inside the message, or in a linked S3 bucket
     * @param SqsMessage $message
     * @return String the payload, either found inside the message, or found in a linked S3 bucket
     */
    function getPayload(SqsMessage $message): ?String
    {
        if ($s3Pointer = $this->sqsExtendedClient->getS3PointerJ3($message)) { // response containing a link to payload stored in S3?
            return $this->s3Client->getObject([
                'Bucket' => $s3Pointer['s3BucketName'],
                'Key' => $s3Pointer['s3Key']
            ])->get("Body"); // return the payload from S3
        } else
            return $message ? $message->getBody() : null; // return the payload directly from the message
    }

    /**
     * used for sending the response back to TMS
     * @param string $respBody
     * @param string $correlationId normally the ID or the corresponding Request Message
     * @return string|null on success, the msg ID is returned, on error null is returned
     */
    function sendResponse(string $respBody, string $correlationId): ?string
    {
        $this->loggerService->logDebug(sprintf("sending response: %s", substr($respBody,0,200)));

        if (!$this->sqsClient) {
            $this->loggerService->logError("Missing credentials for AWS SQS");
            return null;
        }

        try {
            $result = $this->sqsExtendedClient->sendLargeMessage($respBody, $this->tpoResponseQueueUrl, $correlationId);
        } catch (AwsException $e) {
            // output error message if fails
            $this->loggerService->logError("ERROR: Failed sending SQS message: " . $e->getMessage());
            return null;
        }
        $sentId = $result->get("MessageId");
        $this->loggerService->logDebug("SQS Response sent back to EDI Requestor, MsgId: " . $sentId . " - correlationId: " . $correlationId);
        return $sentId;
    }

    /**
     * Wait max 10 seconds. Then return between 0 and 5 messages or null in case of error
     * @return Result|null returns between 0 and 5 messages or null in case of error
     */
    function listenForTpoRequests(): ?Result
    {
        try {
            $result = $this->sqsClient->receiveMessage([
                'QueueUrl' => $this->tpoRequestQueueUrl,
                'WaitTimeSeconds' => 10,
                'MaxNumberOfMessages' => 5,
                "MessageAttributeNames" => ["correlationId", "ExtendedPayloadSize"]
            ]);
        } catch (Exception $e) {
            $this->loggerService->logError("ERROR: Failed receiving SQS responses: " . $e->getMessage());
            return null;
        }
        return $result;
    }


    private function createSqsStringAttr(String $attrName, String $attrVal): string
    {
        return "$attrName => ['DataType' => 'String', 'StringValue' => $attrVal]";
    }

    private function createSqsNumberAttr(String $attrName, int $attrVal): string
    {
        return "$attrName => ['DataType' => 'Number', 'StringValue' => strval($attrVal)]";
    }

    /*function createSqsBinaryAttr(String $attrName, String $attrVal): array
    {
        return [$attrName => ['DataType' => 'Binary', 'BinaryValue ' => $attrVal]];
    }*/
    function deleteTpoRequest(SqsMessage $request)
    {
        $this->sqsClient->deleteMessage(['QueueUrl' => $this->tpoRequestQueueUrl, 'ReceiptHandle' => $request->getReceiptHandle()]);
    }
}
