<?php
/**
 * Created by PhpStorm.
 * User: Jens
 * Date: 14.02.2019
 * Time: 13:51
 */

namespace App\Service;

use DateInterval;
use DateTime;
use DateTimeZone;
use Exception;

class ToolsService
{
    static function unixTimeToStr(int $time = null): string
    {
        if ($time === null)
            return "null";
        try {
            return (new DateTime('@' . $time, timezone_open('UTC')))->format('Y-m-d H:i:s');
        } catch (Exception $e) {
            return "*invalid*";
        }
    }

    static function dateTimeFromInt(int $time = null): ?DateTime
    {
        if ($time === null)
            return null;
        try {
            return new DateTime('@' . $time, timezone_open('UTC'));
        } catch (Exception $e) {
            echo "ERROR !!! unixTimeToDateTime failed:\n" . $e->getMessage();
            return null;
        }
    }

    static function dateTimeToStr(DateTime $dt = null): string // The declaration can be made to accept NULL values if the default value of the parameter is set to NULL
    {
        return ($dt !== null ? $dt->format('Y-m-d H:i:s') : "null");
    }

    /**
     * returns a String for the current DateTime (default: UTC)
     * @param bool $dateOnly return only Date
     * @param bool $localTime return localTime instead of UTC
     * @param bool $forFileNames return without ' ' and ':' so the string can be used as part of a filename
     * @return string the current DateTime as a string
     */
    static function nowToStr(bool $dateOnly = false, bool $localTime = false, bool $forFileNames = false): string
    {
        $retStr = self::now($localTime)->format($dateOnly ? 'Y-m-d' : 'Y-m-d H:i:s');

        return $forFileNames ? str_replace([" ", ":"], ["_", ""], $retStr) : $retStr;
    }

    static function convertToUtc(DateTime $inValue = null)
    {
        if ($inValue === null)
            return null;
        try {
            $inValue->add(new DateInterval('PT' . $inValue->getOffset() . 'S'));
        } catch (Exception $e) {
            return null;
        }
        $inValue->setTimezone(new DateTimeZone('UTC'));
        return $inValue;
    }

    /**
     * returns a DateTime Object with Timezone UTC or local
     * @param bool $localTime UTC or local time
     * @return DateTime|null
     */
    static function now(bool $localTime = false): ?DateTime
    {
        try {
            return new DateTime('now', $localTime ? null : timezone_open('UTC'));
        } catch (Exception $e) {
            return null;  // will never happen
        }
    }

    static function ensureRange($value, $min, $max)
    {
        return min(max($value, $min), $max);
    }

    static function inRange($value, $min, $max): bool
    {
        return ($value >= $min) && ($value <= $max);
    }

    static function isJson($string):bool {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }

    static function upDir(int $levels): string
    {
        $retStr = __DIR__ . DIRECTORY_SEPARATOR;
        for ($i = 0; $i < $levels; $i++)
            $retStr .= '..' . ($i < $levels - 1 ? DIRECTORY_SEPARATOR : '');
        return $retStr;
    }

    /**
     * @param $fileContent
     * @param string $fullPath
     * @return bool true = success, false = fail
     */
    static function createFile($fileContent, string $fullPath)
    {
        $pdf = fopen($fullPath, "w") or die("Unable to open file!");
        $result = fwrite($pdf, $fileContent);
        if ($result)
            $result = fclose($pdf);
        return $result;
    }

    static function echoStringAsHexDump(string $str)
    {
        $humanReadable = "";
        for ($i = 0; $i < strlen($str); $i++) {
            if ($i % 16 === 0) {
                echo "  $humanReadable\n" . str_pad(dechex($i), 4, '0', STR_PAD_LEFT) . ": ";
                $humanReadable = "";
            }
            $val = ord($str[$i]);
            echo str_pad(dechex($val), 2, '0', STR_PAD_LEFT) . " ";
            $humanReadable .= ($val === 10 || $val === 13) ? "" : $str[$i]; // do not print CR or LF
        }
        echo str_repeat("   ",16 - ($i-1)%16-1) ."  $humanReadable\n\n";
    }

    static function showStackTrace(LoggerService $logger, Exception $e, int $traceDepth = 20)
    {
        $logger->logDebug(str_repeat("*", 100));
        $logger->logDebug(str_repeat("-", 100));
        $logger->logDebug("now showing trace");
        $logger->logDebug(str_repeat("-", 100));
        $logger->logDebug(str_repeat("*", 100));
        $depth = 0;
        foreach ($e->getTrace() as $trace) {
            $logger->logDebug("ERROR! Trace: " . json_encode($trace));
            if (++$depth >= $traceDepth)
                break;
        }
    }
}
