<?php

namespace App\Service;

use App\Model\SqsMessage;
use Aws\Result;
use Aws\S3\S3Client;
use Aws\Sqs\SqsClient;
use Ramsey\Uuid\Uuid;

/**
 * Class SqsClient.
 *
 * @package AwsExtended
 */
class SqsClientExtended
{
    const IF_NEEDED       = 'IF_NEEDED';
    const ALWAYS          = 'ALWAYS';
    const NEVER           = 'NEVER';

    /** @var String */
    protected $useS3When; // either IF_NEEDED, ALWAYS or NEVER

    /** @var \Aws\Sqs\SqsClient */
    protected $sqsClient; // standard SQS client

    /** @var \Aws\S3\S3Client */
    protected $s3Client; // S3 client used for storing large messages in bucket

    /** @var String */
    protected $s3BucketName; // S3 bucket name

    /** @var LoggerService */
    private $loggerService;

    public function __construct(LoggerService $logger)
    {
        $this->loggerService = $logger;
    }

    /**
     * @param SqsClient $sqs SQS Client
     * @param S3Client $s3 S3 Client
     * @param String $s3Bucket bucket name
     * @param String $useS3When either IF_NEEDED, ALWAYS or NEVER
     */
    function initSqsExtendedClient(SqsClient $sqs, S3Client $s3, string $s3Bucket, string $useS3When)
    {
        $this->sqsClient = $sqs;
        $this->s3Client = $s3;
        $this->s3BucketName = $s3Bucket;
        $this->useS3When = $useS3When;
    }

    /**
     * will send off the message body.
     * Either directly inside the SQS message, or storing in S3 bucket and then passing a pointer to the bucket over SQS.
     * Whether "directly" or "via bucket" is decided by message size
     * and by the setting "useS3When", which is either IF_NEEDED, ALWAYS or NEVER
     * @param String $msgBody
     * @param String $queueUrl
     * @param String $correlationId used when responding to requests. If available, will be added as message attribute
     * @param array $existingPayloadAttr // in case we are forwarding an already existing S§ pointer, the attr needs to be copied also to the fwd msg
     * @return \Aws\Result
     */
    function sendLargeMessage(string $msgBody, string $queueUrl, string $correlationId = null, array $existingPayloadAttr = null): ?Result
    {
        if (!$this->sqsClient) {
            $this->loggerService->logError("ERROR: SqsClientExtended not initialized. Did you forget to call initSqsExtendedClient()?");
            return null;
        }

        switch ($this->useS3When) {
            case self::ALWAYS:
                $useS3 = TRUE;
                break;
            case self::IF_NEEDED:
                $useS3 = $this->isTooBig($msgBody);
                break;
            case self::NEVER:
            default:
                $useS3 = FALSE;
                break;
        }
        $useS3 = $useS3 && $this->s3BucketName;
        $this->loggerService->logDebug("Sending PAYLOAD " . ($useS3 ? "via S3 bucket" : "directly"));
        if ($useS3) {
            // First send the object to S3. The modify the message to store an S3
            // pointer to the message contents.
            $key = $this->generateUuid();
            $receipt = $this->s3Client->upload(
                $this->s3BucketName,
                $key,
                $msgBody
            );
            // Swap the message for a pointer to the actual message in S3.
            $pointer = ['s3BucketName' => $this->s3BucketName, 's3Key' => $key];
            $msgBody = json_encode(["software.amazon.payloadoffloading.PayloadS3Pointer", $pointer]);
        }
        $sendMsgArgs =
            [
                'QueueUrl' => $queueUrl,
                'MessageBody' => $msgBody
            ];
        if ($useS3 || $correlationId || $existingPayloadAttr) {
            $sendMsgArgs['MessageAttributes'] = [];
            if ($existingPayloadAttr) // we're just forwarding a bucket link, make sure the attr is set again
                $sendMsgArgs['MessageAttributes']['ExtendedPayloadSize'] = $existingPayloadAttr;
            elseif ($useS3)
                $sendMsgArgs['MessageAttributes']['ExtendedPayloadSize'] = ['DataType' => 'Number', 'StringValue' => strlen(($msgBody))];
            if ($correlationId)
                $sendMsgArgs['MessageAttributes']['correlationId'] = ['DataType' => 'String', 'StringValue' => $correlationId];
            $this->loggerService->logDebug("MessageAttributes: " . json_encode($sendMsgArgs['MessageAttributes'], JSON_PRETTY_PRINT));
        }

        return $this->sqsClient->sendMessage($sendMsgArgs);
    }

    /**
     * Checks if the received message contains ready-to-use (intrinsic) payload,
     * or if it contains a link to payload stored in an S3 Bucket
     * @param SqsMessage $msg The message to be checked
     * @return array|null returns a pointer to an S3 bucket,
     * or null if the message contains intrinsic payload
     */
    function getS3PointerJ3(SqsMessage $msg): ?array
    {
        $result = json_decode($msg->getBody(), true);
// if present, the $s3PointerArray will be at 2nd position in the $result-array
// and will  have the form ['s3BucketName' => "x", 's3Key' => "y"]
        $s3PointerArray = (is_array($result) && array_key_exists(1, $result)) ? $result[1] : null;
        $isS3Pointer = is_array($s3PointerArray) &&
                       count($s3PointerArray) == 2 &&
                       empty(array_diff_key($s3PointerArray, ['s3BucketName' => "dummy", 's3Key' => "dummy"])); // compare the array keys
        $this->loggerService->logDebug("Receiving PAYLOAD " . ($isS3Pointer ? "via S3 bucket" : "directly"));
        return $isS3Pointer ? $s3PointerArray : null;
    }

    private function isTooBig($message)
    {
        // strlen returns number of bytes, not number of characters
        // e.g. strlen("Jens") is 4 and strlen("Ærø") is 5
        $maxSize = isset($_SERVER['MAX_SQS_SIZE_KB']) ? intval($_SERVER['MAX_SQS_SIZE_KB']) : 255; // as of this size, we will move the payload to an S3 Bucket, and link it in the SQS message
        $maxSize = ToolsService::inRange($maxSize, 1, 255) ? $maxSize : 255; // accept env values from 1..255, else set = 255
        return strlen($message) > $maxSize * 1024;
    }

    /**
     * Generate a UUID v4.
     * @return string The uuid.
     */
    private function generateUuid()
    {
        return Uuid::uuid4()->toString();
    }

}
