<?php
/**
 * Created by PhpStorm.
 * User: Jens
 * Date: 3.03.2020
 * Time: 11:51
 */

namespace App\Service;

use App\Model\CustomsSummary;
use DateTime;
use DateTimeZone;
use Exception;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class DhlService
{
    const PAYMENT_INFO_VALUES = [ // payment info in REST v1, incoterm in REST v2
        "CFR" => "Cost And Freight",
        "CIF" => "Cost, Insurance and Freight",
        "CIP" => "Carriage And Insurance Paid To",
        "CPT" => "Carriage Paid To",
        "DAF" => "Delivered At Frontier",
        "DDP" => "Delivery Duty Paid",
        "DDU" => "Delivery Duty Unpaid",
        "DAP" => "Delivered At Place",
        "DEQ" => "Delivered Ex Quay (Duty Paid)",
        "DES" => "Delivered Ex Ship",
        "EXW" => "Ex Works",
        "FAS" => "Free Alongside Ship",
        "FCA" => "Free Carrier",
        "FOB" => "Free On Board"
    ];

    /* DHL expects one of the following Inco terms:
       EXW ExWorks, FCA Free Carrier, CPT Carriage Paid To, CIP Carriage and Insurance Paid To, DPU Delivered at Place Unloaded, DAP Delivered at Place
       DDP Delivered Duty Paid, FAS Free Alongside Ship, FOB Free on Board, CFR Cost and Freight, CIF Cost, Insurance and Freight */
    const INCO_TERMS_TMS_CODING = [ // definition taken from TMS table tms_incoterm
        "", // 0 is not defined in TMS
        "FCA",
        "FAS",
        "FOB",
        "CFR",
        "CIF",
        "DAP",// DAT in TMS is mapped to DAP at DHL
        "DAP",
        "CPT",
        "CIP",
        "DDP"
    ];

//    const BENTLEY_ACCOUNT_OUTBOUND          = "421770867"; // from Barbara, 10Dec2020. Was one of the four numbers from Nic
//    const BENTLEY_ACCOUNT_INBOUND_3RD_PARTY = "967758271"; // or  one of these: 421783047, 967759584. Barbara: "For third party or inbound use the account number, you have"

    private $strictMode; // if true, throws exception immediately when data is missing. Unless TPO_STRICT_MODE explicitly set false in ENV, we will be strict

    /** @var LoggerService */
    private $loggerService;

    /**
     * @var HttpClientInterface
     */
    private $httpClient;

    private $dhlUrl;

    private $testing;

    public function __construct(HttpClientInterface $client, LoggerService $logger) {
        $this->loggerService = $logger;
        $this->strictMode    = !(isset($_SERVER['TPO_STRICT_MODE']) && $_SERVER['TPO_STRICT_MODE'] == 'false'); // unless explicitly set false, we will be strict
        $this->httpClient    = $client;
        $this->dhlUrl        = $_SERVER['DHL_URL'];
        $this->testing       = (isset($_SERVER['TESTING']) && $_SERVER['TESTING'] == 'true');
    }

    /**
     * @throws Exception
     */
    private function getAccountCode($org, $dest): string {
        if (empty($_SERVER['ACCOUNT_CODE_UK_DOMESTIC'])
            || empty($_SERVER['ACCOUNT_CODE_UK_EXPORT'])
            || empty($_SERVER['ACCOUNT_CODE_UK_IMPORT'])
            || empty($_SERVER['ACCOUNT_CODE_NON_UK'])
        )
            throw new Exception("Account codes not defined");

        if (trim($org->entityAddress->country->iso) === "GB")
            return trim($dest->entityAddress->country->iso) === "GB" ?
                $_SERVER['ACCOUNT_CODE_UK_DOMESTIC'] :
                $_SERVER['ACCOUNT_CODE_UK_EXPORT'];
        else
            return trim($dest->entityAddress->country->iso) === "GB" ?
                $_SERVER['ACCOUNT_CODE_UK_IMPORT'] :
                $_SERVER['ACCOUNT_CODE_NON_UK'];
    }

    /**
     * In many cases in Sirius, the incoterm is not defined.
     * If not defined, we need to throw an error, since DHL requires the incoterm to be defined
     * @param $tpoObj
     * @throws Exception
     */
//    public function getIncoterm($tpoObj): string {
    /* DHL expects one of the following Inco terms:
       EXW ExWorks, FCA Free Carrier, CPT Carriage Paid To, CIP Carriage and Insurance Paid To, DPU Delivered at Place Unloaded, DAP Delivered at Place
       DDP Delivered Duty Paid, FAS Free Alongside Ship, FOB Free on Board, CFR Cost and Freight, CIF Cost, Insurance and Freight */
//        if (!isset($tpoObj->customs) || empty($tpoObj->customs->tms_incoterm_id)) // incoterm == 0 is not allowed
//            $this->strictError("Incoterm needs to be defined on customs tab");
//        if ($tpoObj->customs->tms_incoterm_id > count(self::INCO_TERMS_TMS_CODING) - 1)
//            $this->strictError("Incoterm has illegal value: " . $tpoObj->customs->tms_incoterm_id);
//        return self::INCO_TERMS_TMS_CODING[$tpoObj->customs->tms_incoterm_id];
//    }

    /**
     * expect package count>0 and waypoint count=2
     * @param $tpoObj
     * @throws Exception if either count not OK
     */
    public function validateInputDataPackagesAndWaypoints($tpoObj) {
        if (!is_array($tpoObj->packaging) || count($tpoObj->packaging) < 1)
            $this->strictError("No Packages found");
        $waypointCount = 0;
        if (!is_array($tpoObj->waypoint) || ($waypointCount = count($tpoObj->waypoint)) != 2)
            $this->strictError("Expecting exactly 2 waypoints (org and dest), found $waypointCount");
        foreach ($tpoObj->waypoint as $wp)
            if (!$this->getWaypointTimeZone($wp))
                $this->strictError("Missing timezone info for waypoint");
    }

    /**
     * @param $tpoObj
     * @throws Exception
     */
    public function validateInputDataForShipmentReq($tpoObj) {
        $this->validateInputDataPackagesAndWaypoints($tpoObj);
        $this->validateSpecificField($tpoObj->waypoint[0]->planned_date, "Planned Date");
        if (!strlen($tpoObj->waypoint[0]->planned_date->date)) { // todo: in some cases the date is empty, in which case we use scheduled date as a fallback (scheduled_date unfortunately has no timezone info)
            $this->validateSpecificField($tpoObj->waypoint[0]->scheduled_date, "Scheduled Date");
            $tpoObj->waypoint[0]->planned_date->date = $tpoObj->waypoint[0]->scheduled_date;
        }
        $this->validateSpecificField($this->getPhoneNumber($tpoObj->waypoint[0]), "Sender Phone Number");
        $this->validateSpecificField($tpoObj->waypoint[0]->contact, "Sender Contact Person");
        $this->validateSpecificField($this->getPhoneNumber($tpoObj->waypoint[1]), "Receiver Phone Number");
        $this->validateSpecificField($tpoObj->waypoint[1]->contact, "Receiver Contact Person");
        $this->validateSpecificField($tpoObj->waypoint[0]->entityAddress->entity->vat_number, "Sender VAT Number"); // not always strictly needed (only if we add the VAT section to our request)
        $this->validateSpecificField($tpoObj->waypoint[1]->entityAddress->entity->vat_number, "Receiver VAT Number");
    }

    /**
     * @param $fieldVal
     * @param $fieldDescription
     * @throws Exception
     */
    public function validateSpecificField($fieldVal, $fieldDescription) {
        if (!$fieldVal) {
            $this->strictError("$fieldDescription missing");
        }
    }

    /**
     * @param $waypoint
     * @return string checking in 4 places for a usable phone number. Returns "" if none found
     */
    function getPhoneNumber($waypoint): string {
        if (property_exists($waypoint, "contact") && property_exists($waypoint->contact, "mobile") && strlen((string) $waypoint->contact->mobile))
            return $waypoint->contact->mobile;
        if (property_exists($waypoint, "contact") && property_exists($waypoint->contact, "phone") && strlen((string) $waypoint->contact->phone))
            return $waypoint->contact->phone;
        if (property_exists($waypoint, "mobile") && strlen((string) $waypoint->mobile))
            return $waypoint->mobile;
        if (property_exists($waypoint, "phone") && strlen((string) $waypoint->phone))
            return $waypoint->phone;
        return "";
    }

    /**
     * generates the body for "POST /shipments" (DHL API REST v2)
     * @param $tpoObj object Transport Order Object (normally parsed from xml from the TMS Backend)
     * @param $productCode string the DHL product code, a one-letter string
     * @return string
     * @throws Exception
     */
    public function generateShipmentReqBody($tpoObj, $productCode): string {
        $customsSummary = $this->calcCustomsSummary($tpoObj);
        $this->loggerService->logDebug(sprintf("customsSummary %s", json_encode($customsSummary)));

//        $isCustomsDeclarable = isset($tpoObj->customs) && !empty($tpoObj->customs->customs_clerance_required);
        $supportPLT = (isset($_SERVER['PLT_SUPPORT']) && $_SERVER['PLT_SUPPORT'] == 'true'); // PLT support, only if explicitly set true
        $this->loggerService->logDebug("PLT support: " . ($supportPLT ? "true" : "false"));
        $addExportDeclaration = $supportPLT && $customsSummary->clearanceRequired;
        $this->loggerService->logDebug("addExportDeclaration: " . ($addExportDeclaration ? "true" : "false"));

        $this->validateInputDataForShipmentReq($tpoObj); // ensure that all data is available before generating DHL Shipment request Body

        $packages    = [];
        $packIndex   = 1;
        $totalWeight = 0;
        foreach ($tpoObj->packaging as $idx => $pack) {
            $quantity = $pack->quantity;
            for ($i = 0; $i < $quantity; $i++) {
                $totalWeight += $pack->weight;
                $packages[]  = $this->createPackage(
                    $pack->weight / $quantity,
                    ceil($pack->length / 10),
                    ceil($pack->width / 10),
                    ceil($pack->height / 10),
                    'Piece ' . ($packIndex)
                );
                $packIndex++;
            }
        }

        $org  = $tpoObj->waypoint[0];
        $dest = $tpoObj->waypoint[1];

        $requestMessage = [
//            "plannedShippingDateAndTime" => str_replace("+", " GMT+", date('c', time() + 7 * 24 * 60 * 60)), // iso8601: "2020-11-20T17:21:16+01:00" // todo: get from xml as soon as info is available
            "plannedShippingDateAndTime" => $this->getDhlDateTimeFromScfDateTime($tpoObj->waypoint[0]->planned_date), // iso8601: "2020-11-20T17:21:16+01:00"
            "pickup" => [
                "isRequested" => false
            ],
            "productCode" => $productCode,
//                "localProductCode" => "S", // localProductCode probably not needed
            "getRateEstimates" => true, // will be part of the response
            "accounts" => [
                [
                    "typeCode" => "shipper", // [shipper, payer, duties-taxes]
                    "number" => $this->getAccountCode($org, $dest)
                ]
            ],
            "customerReferences" => [
                [
//                    "value" => $tpoObj->reference_number,
                    "value" => $tpoObj->shipment_id,
                    "typeCode" => "CU"
                ]
            ],
            /*  "identifiers" => [
                  [
                      "typeCode" => "shipmentId",
                      "value" => "1111111111"
                  ]
              ],*/  // "400101: Use of own Piece Identification Number and/or Shipment Identification Number is not allowed for your Username. If required then please contact your DHL representative.",
            "customerDetails" => [
                "shipperDetails" => $this->createFullAddress($org),
                "receiverDetails" => $this->createFullAddress($dest)
            ],
            "content" => [
                "packages" => $packages,
                "isCustomsDeclarable" => $customsSummary->clearanceRequired,
                "description" => $this->getBestOverallShipmentDescription($tpoObj),
                "unitOfMeasurement" => "metric"
            ],

            "shipmentNotification" => $this->createEmailNotificationBlock($tpoObj),
            "getOptionalInformation" => true
        ];

        if ($customsSummary->clearanceRequired) {
            $requestMessage["customerDetails"] += [
                "exporterDetails" => $this->createFullAddress($org), // todo, get from new fields in Sirius GUI
                "importerDetails" => $this->createFullAddress($dest) // todo, get from new fields in Sirius GUI
            ];
            if (isset($tpoObj->client->entityAddress))
                $requestMessage["customerDetails"] += [
                    "payerDetails" => $this->createFullAddress($tpoObj->client, $customsSummary->eoriNumber) // todo, get from new fields in Sirius GUI
                ];
            $requestMessage["content"]         += [
                "declaredValue" => +$customsSummary->totalValue,
                "declaredValueCurrency" => $customsSummary->currency,
                "incoterm" => $customsSummary->incoterm, // [ EXW, FCA, CPT, CIP, DPU, DAP, DDP, FAS, FOB, CFR, CIF ]
            ];

            // the following three objects are not part of the DHL API spec, but hinted in EU-VAT-Bespoke-Solution-Requirement-MyDHL-API.pdf
//            $requestMessage["ShipmentInfo"]        = $this->createShipmentInfo($tpoObj->customs->declared_currency);
//            $requestMessage["PaymentInfo"]         = $this->getIncoterm($tpoObj);
//            $requestMessage["InternationalDetail"] = $this->createInternationalDetail($tpoObj->customs->incoterm_place, $totalWeight, $tpoObj->reference_number, $tpoObj->date);

        }
        if ($addExportDeclaration) {
            $requestMessage["content"] += $this->getExportDeclarationData($tpoObj, $customsSummary->incotermPlace); // append
            $requestMessage            += $this->renderPltInvoiceInstructions(); // append
        }
        return json_encode($requestMessage, JSON_UNESCAPED_UNICODE);
    }

    /**
     * we will send email including certain references to offer_responsible_person_email and order_person_email
     * in case the two email addresses are identical, we will only send one email message
     * @param $tpoObj
     * @return array
     */
    private function createEmailNotificationBlock($tpoObj): array {
        $bespokeMessage = "ShipID: " . $tpoObj->reference_number;
        if (isset($tpoObj->client_reference) && strlen($tpoObj->client_reference))
            $bespokeMessage .= " ; Client Reference: " . $tpoObj->client_reference;
        if (isset($tpoObj->additional_reference) && strlen($tpoObj->additional_reference))
            $bespokeMessage .= " ; Additional Reference: " . $tpoObj->additional_reference;

        $notifyEmail = [];
        if (isset($tpoObj->offer_responsible_person_email) && strlen($tpoObj->offer_responsible_person_email))
            $notifyEmail[] = $tpoObj->offer_responsible_person_email;
        if (isset($tpoObj->order_person_email) && strlen($tpoObj->order_person_email))
            $notifyEmail[] = $tpoObj->order_person_email;
        $notifyEmail       = array_unique($notifyEmail); // get rid of duplicates
        $notificationBlock = [];
        foreach ($notifyEmail as $email)
            $notificationBlock[] = [
                "typeCode" => "email",
                "receiverId" => $email,
                "languageCode" => "eng",
                "languageCountryCode" => "en",
                "bespokeMessage" => $bespokeMessage
            ];
        return $notificationBlock;
    }

    /**
     * Unfortunately the xml uses different formats for waypoints when requesting rates and booking a shipment.
     * rateReq: planned_date and scheduled_date are just strings, timezone has its own element
     * booking: planned_date is an object including timezone, scheduled date is just a string. No own timezone element
     * @param $waypoint
     * @return string|null
     */
    private function getWaypointTimeZone($waypoint): ?string {
        return $waypoint->timezone ?? (is_object($waypoint->planned_date) ? $waypoint->planned_date->timezone : null);
    }

    /**
     * generates the body for "POST /rates" (DHL API REST v2)
     * @param $tpoObj object Transport Order Object (normally parsed from xml from the TMS Backend)
     * @throws Exception
     */
    public function generateRatesReqBody($tpoObj): string {
        $customsSummary = $this->calcCustomsSummary($tpoObj);
        $this->loggerService->logDebug(sprintf("customsSummary %s", json_encode($customsSummary)));

        $this->validateInputDataPackagesAndWaypoints($tpoObj); // ensure that all data is available before generating DHL Rating request Body

        $packages = [];
        foreach ($tpoObj->packaging as $pack) {
            $quantity = $pack->quantity;
            for ($i = 0; $i < $quantity; $i++) {
                $packages[] = $this->createPackage(
                    $pack->weight / $quantity,
                    ceil($pack->length / 10),
                    ceil($pack->width / 10),
                    ceil($pack->height / 10)
                );
            }
        }

        $org  = $tpoObj->waypoint[0];
        $dest = $tpoObj->waypoint[1];

        $shipDate = $this->getDhlDateTimeFromScfDateTime((object) [
            "date" => $org->scheduled_date,
            "timezone" => $this->getWaypointTimeZone($org)
        ]);
        $this->loggerService->logDebug(sprintf("j3 shipdate %s", $shipDate));

        $requestMessage = [
            "customerDetails" => [
                "shipperDetails" => $this->createSimpleAddress($org->entityAddress, true),
                "receiverDetails" => $this->createSimpleAddress($dest->entityAddress, true)
            ],
            "plannedShippingDateAndTime" => $shipDate, // DHL format: "2020-11-20T17:21:16 GMT+01:00"
            "unitOfMeasurement" => "metric",
            "isCustomsDeclarable" => $customsSummary->clearanceRequired,
            "packages" => $packages,
            "accounts" => [
                [
                    "typeCode" => "shipper", // enum:  shipper, payer, duties-taxes
                    "number" => $this->getAccountCode($org, $dest)
                ]
            ],
            "valueAddedServices" => [
            ],
            "payerCountryCode" => "GB",
//            "pickup" => [
//                "isRequested" => false
//            ],
            "requestAllValueAddedServices" => false,
            "returnStandardProductsOnly" => true, // non-standard products need DHL Express special customer agreement
            "nextBusinessDay" => true, // we will also get offers for Sat and Sunday shipments, however they will often be too late. The frontend then needs to filter
            "productTypeCode" => "all"
        ];

        if ($customsSummary->clearanceRequired) {
            $requestMessage["monetaryAmount"] = [
                [
                    "typeCode" => "declaredValue",
                    "value" => +$customsSummary->totalValue,
                    "currency" => $customsSummary->currency
                ]
            ];
        }
        return json_encode($requestMessage, JSON_UNESCAPED_UNICODE);
    }

    /**
     * @param $scfDateTime // object, e.g. {"timezone":"Europe\/Berlin","timezone_type":"3","date":"2019-10-05 12:00:00"}
     * @return string // e.g. "2019-10-05T12:00:00 GMT+02:00"
     * @throws Exception
     */
    public function getDhlDateTimeFromScfDateTime($scfDateTime): string {
        $dateTime = new DateTime($scfDateTime->date, new DateTimeZone($scfDateTime->timezone));

        // If the time is in the past, and if we are testing, we'll set the time to 24 hrs ahead
        if ($this->testing && ($dateTime->getTimestamp() < (new DateTime())->getTimestamp()))
            $dateTime->setTimestamp((new DateTime())->getTimestamp() + 24 * 60 * 60);

        return str_replace("+", " GMT+", $dateTime->format('c')); // iso8601: "2020-11-20T17:21:16+01:00" - DHL: "2020-11-20T17:21:16 GMT+01:00"
    }


    public function createPackage(float $weight, int $l, int $w, int $h, string $custRef = null): array {
        $retArray = [
//            "typeCode" => "2BP", // optional [ 3BX, 2BC, 2BP, CE1, 7BX, 6BX, 4BX, 2BX, 1CE, WB1, WB3, XPD, 8BX, 5BX, WB6, TBL, TBS, WB2 ]
            "weight" => round($weight, 3),
            "dimensions" => [
                "length" => round($l, 2),
                "width" => round($w, 2),
                "height" => round($h, 2)
            ]
        ];
        if ($custRef && strlen($custRef) > 0) {
            $retArray["customerReferences"] = [
                [
                    "value" => $custRef,
                    "typeCode" => "CU"
                ]
            ];
        }
        return $retArray;
    }

    /**
     * As ShipmentInfo isn't specified yet, I implemented it as best guess based on
     * EU-VAT-Bespoke-Solution-Requirement-MyDHL-API.pdf
     * @param string $currency
     * @return array
     */
    public function createShipmentInfo(string $currency): array {
        return [
            "Currency" => $currency,
            "UnitOfMeasurement" => "SI",
            "LabelOptions" => ["DHLCustomsInvoiceType" => "COMMERCIAL_INVOICE"]
        ];
    }

    /**
     * As InternationalDetail isn't specified yet, I implemented it as best guess based on
     * EU-VAT-Bespoke-Solution-Requirement-MyDHL-API.pdf
     * @param string $incoPlace
     * @param float $totalGrossWeight
     * @param string $invoiceNo
     * @param string $invoiceDate
     * @return array
     */
    public function createInternationalDetail(string $incoPlace, float $totalGrossWeight, string $invoiceNo, string $invoiceDate): array {
        return [
            "ExportDeclaration" => [
                "PlaceOfIncoTerm" => $incoPlace,
                "InvoiceTotalGrossWeight" => number_format($totalGrossWeight, 2, ".", ""),
                "InvoiceNumber" => $invoiceNo,
                "InvoiceDate" => $invoiceDate
            ],
            "Commodities" => ["CustomsValue" => "COMMERCIAL_INVOICE"]
        ];
    }

    public function createFullAddress($wayPoint, $eoriNumber=null) {
        $firstName = property_exists($wayPoint->contact, "first_name") ? $wayPoint->contact->first_name : "";
        $lastName  = property_exists($wayPoint->contact, "last_name") ? $wayPoint->contact->last_name : "";
        if (empty($firstName) & empty($lastName))
            $name = "unknown"; // decided to use "unknown" instead of throwing exception
        else
            $name = trim("$firstName $lastName");

        $retArray = [
            "postalAddress" => $this->createSimpleAddress($wayPoint->entityAddress),
            "contactInformation" => [
                "phone" => preg_replace("/ +/", "", $this->getPhoneNumber($wayPoint)), // remove spaces
                "companyName" => $wayPoint->entityAddress->entity->entity_lgl_name,
                "fullName" => $name
            ],
            "registrationNumbers" => [ // this block is optional, but if added all 3 fields must have a value.
                [
                    "typeCode" => "VAT",
                    "number" => $wayPoint->entityAddress->entity->vat_number,
                    "issuerCountryCode" => trim($wayPoint->entityAddress->country->iso),
                ]
            ],
            "typeCode" => "business"
        ];
        if ($eoriNumber) {
            $retArray["registrationNumbers"][] = [
                "typeCode" => "EOR",
                "number" => $eoriNumber,
                "issuerCountryCode" => substr($eoriNumber, 0, 2),
            ];
        }

        if (property_exists($wayPoint->contact, "email"))
            $retArray['contactInformation']["email"] = $wayPoint->contact->email;

        return $retArray;
    }

    /**
     * @param $entityAddress
     * @param false $ignoreStreetInfo if true, only zip, city and country are included
     * @return array
     */
    public function createSimpleAddress($entityAddress, $ignoreStreetInfo = false): array {
        $retArray = [
            "postalCode" => $entityAddress->postcode,
            "cityName" => $entityAddress->city,
            "countryCode" => trim($entityAddress->country->iso)
        ];
        if (!$ignoreStreetInfo) {
            $retArray["addressLine1"] = $entityAddress->street1;
            // now add optional information (optional information is not allowed to be empty (at least in some cases))
            if ($entityAddress->street2) // optional
                $retArray["addressLine2"] = $entityAddress->street2;
        }
        return $retArray;
    }

    /**
     * returns the fields needed for making DHL render a pdf invoice for PLT (Paperless Trade)
     * DHL will mainly use the information from the "exportDeclaration" block to generate the invoice
     * The pdf will be part of the Shipment Request response
     * NOTE: This functionality was implemented in order to get the DHL PLT Production approval 15.12.20.
     * NOTE: We might decide not to use this fcty in production, and instead attach our own invoice as pdf to the shipment request
     * @return array
     */
    public function renderPltInvoiceInstructions(): array {
        return ["valueAddedServices" => [
            [
                "serviceCode" => "WY"
            ]
        ],
            "outputImageProperties" => [
                "encodingFormat" => "pdf",
                "imageOptions" => [
                    [
                        "typeCode" => "invoice", // [ label, waybillDoc, invoice, receipt, qr-code ]
                        "isRequested" => true, // To be used for waybillDoc, invoice, receipt and QRcode. If set to true then the document is provided otherwise not
                        "invoiceType" => "commercial" // [ commercial, proforma ]
                    ]
                ]
            ]
        ];
    }

    /**
     * @param $tpoObj object Transport Order Object (normally parsed from xml from the TMS Backend)
     * @return array
     */
    private function getExportDeclarationData($tpoObj, $incoPlace): array {
        $lineItems = [];
        $lineIndex = 1;

        foreach ($tpoObj->customs->packages as $idx => $pack) {
            $quantity    = +$pack->quantity;
            $priceUnit   = +$pack->price_unit ?? 1; // price refers to x units, e.g. price for 1000 bolts
            $lineItems[] = [
                "number" => $lineIndex,
                "description" => $pack->description,
                "price" => round(+$pack->net_price / $priceUnit, 3),
                "priceCurrency" => $pack->currency_iso,
                "quantity" => [
                    "value" => $quantity,
                    "unitOfMeasurement" => "PCS" // [ BOX, 2GM, 2M, 2M3, 3M3, M3, DPR, DOZ, 2NO, PCS, GM, GRS, KG, L, M, 3GM, 3L, X, NO, 2KG, PRS, 2L, 3KG, CM2, 2M2, 3M2, M2, 4M2, 3M ]
                ],
                "commodityCodes" => [
                    [
                        "typeCode" => "outbound", // [ outbound, inbound ]
                        "value" => $pack->customs_tariff_number // e.g HS1234567890, https://www.trade-tariff.service.gov.uk/sections
                    ]
                ],
                "exportReasonType" => "permanent",
                "manufacturerCountry" => "DE", // todo: missing in xml
//                        "exportControlClassificationNumber" => "US123456789",
                "weight" => [
                    "netValue" => round(+$pack->part_net_weight * $quantity, 3), // todo: multiply by quantity?
                    "grossValue" => round(+$tpoObj->packaging[$idx]->weight, 3) // todo: double check with quantity != 1. Also check net and gross
                ]
            ];
            $lineIndex++;
        }
        return [
            "exportDeclaration" => [
                "lineItems" => $lineItems,
                "invoice" => [
                    "number" => $tpoObj->reference_number,
//                    "date" => date("Y-m-d"),
                    "date" => $tpoObj->date,
//                  "signatureName" => "Empfänger", // missing in xml, but optional
//                  "signatureTitle" => "Mrs.", // missing in xml, but optional
//                  "signatureImage" => base64_encode(file_get_contents("../assets/dummySignature.png")) optional, if we want o use it, we need to decide which signature to use and then UNCOMMENT
                ],
                "remarks" => [
                    [
                        "value" => "remark" // todo: any info we want to put here?
                    ],
                ],
                "placeOfIncoterm" => $incoPlace,
                "shipmentType" => "commercial"
            ]
        ];
    }

    /**
     * We need an overall description for the shipment.
     * We start by looking at customs declarations if available, and pick the description for the line item with the highest value.
     * If not successful, we look for a part name
     * If also not successful, we look for a package description
     * As a last fallback, we return the string "Shipment Description"
     * @param $tpoObj
     * @return string
     */
    private function getBestOverallShipmentDescription($tpoObj): string {
        $description     = "";
        $highestValSoFar = -1e9;
        if (isset($tpoObj->customs) && isset($tpoObj->customs->packages)) {
            foreach ($tpoObj->customs->packages as $pack) {
                $priceUnit          = +$pack->price_unit ?? 1; // price refers to x units, e.g. price for 1000 bolts
                $price              = round(+$pack->net_price / $priceUnit, 3);
                $lineItemTotalValue = $price * $pack->quantity;
                if ($lineItemTotalValue > $highestValSoFar) {
                    $highestValSoFar = $lineItemTotalValue;
                    if (!empty($pack->description))
                        $description = $pack->description;
                }
            }
        }
        if (!empty($description))
            return $description; // the line item with the highest declared value

        if (isset($tpoObj->partnumber) && is_array($tpoObj->partnumber)) {
            foreach ($tpoObj->partnumber as $part) {
                if (!empty($part->name)) {
                    return $part->name; // the first found part with a non-empty name
                }
            }
        }

        if (isset($tpoObj->packaging) && is_array($tpoObj->packaging)) {
            foreach ($tpoObj->packaging as $pack) {
                if (!empty($pack->description)) {
                    return $pack->description; // the first found package with a non-empty decription
                }
            }
        }

        return empty($description) ? "Shipment Description" : $description;
    }


    /**
     * Throws exception if in STRICT MODE, otherwise just logs the event preceded by the word: "UNSTRICT:"
     * @param string $errorStr
     * @throws Exception
     */
    private function strictError(string $errorStr) {
        if ($this->strictMode)
            throw new Exception($errorStr);
        else
            $this->loggerService->logNotice("UNSTRICT: " . $errorStr); // would have thrown exception if we had been in STRICT Mode
    }

    /**
     * POST /shipment  to DHL REST API v2
     * @param string $jsonBody
     * @return string the response content (json string) or a simple error string
     * @throws Exception
     * @throws TransportExceptionInterface
     */
    public function postShipmentRequest(string $jsonBody): string {
        $url = $this->dhlUrl . "shipments";
        $this->loggerService->logDebug("Calling DHL endpoint " . $url);
        $this->loggerService->logDebug("Calling DHL endpoint, BODY " . $jsonBody);
        try {
            $response = $this->httpClient->request('POST', $url, [
                'headers' => $this->getHttpHeaders(),
                'body' => $jsonBody
            ]);
        } catch (TransportExceptionInterface $e) {
            $this->loggerService->logDebug("TransportExceptionInterface" . $e->getMessage());
        }
        if (floor($response->getStatusCode() / 100) != 2) { // if not a 2xx status
            $errorString = "POST ShipmentRequest failed, http status: " . $response->getStatusCode() . " -> FAIL INFO: " . $response->getContent(false);
            $this->loggerService->logError($errorString);
            return $errorString;
        }
        return $response->getContent(); // success
    }

    /**
     * @param string $jsonBody
     * @return string
     * @throws Exception|TransportExceptionInterface
     */
    public function postRateRequest(string $jsonBody): string {
        $url = $this->dhlUrl . "rates";
        $this->loggerService->logDebug("Calling DHL endpoint " . $url);
        $this->loggerService->logDebug("Posting body: " . $jsonBody);
        $response = $this->httpClient->request('POST', $url, [
            'headers' => $this->getHttpHeaders(),
            'body' => $jsonBody
        ]);

        $this->loggerService->logDebug("POST itself did not throw an exception: ");
//        echo("POST itself did not throw an exception, here the response body: " . $response->getContent(false));

        if (floor($response->getStatusCode() / 100) != 2) { // if not a 2xx status
            $errorString = "POST RateRequest failed, http status: " . $response->getStatusCode() . " -> FAIL INFO: " . $response->getContent(false);
            $this->loggerService->logError($errorString);
            return $errorString;
        }
        return $response->getContent(); // success
    }

    /**
     * @param $tpoObj object Transport Order Object (normally parsed from xml from the TMS Backend)
     * @return CustomsSummary
     * @throws Exception
     */
    private function calcCustomsSummary($tpoObj): CustomsSummary {
        $summary = new CustomsSummary();

        // todo: used to be an array, but that shouldn't happen anymore
        $customsObj = (is_array($tpoObj->customs) && count($tpoObj->customs) > 0) ? $tpoObj->customs[0] : $tpoObj->customs;

        if (is_object($customsObj)) {
            $this->loggerService->logDebug(sprintf("customs[0] %s", json_encode($customsObj)));
            $incoTermId = $customsObj->tms_incoterm_id;
            if (!$incoTermId) // incoterm undefined or 0 is not allowed
                $this->strictError("Incoterm needs to be defined on customs tab");
            if ($incoTermId > count(self::INCO_TERMS_TMS_CODING) - 1)
                $this->strictError("Incoterm has illegal value: " . $incoTermId);
            $summary->incoterm      = self::INCO_TERMS_TMS_CODING[$incoTermId];
            $summary->incotermPlace = $customsObj->incoterm_place;
            $summary->eoriNumber = $customsObj->eori_number;

            $this->loggerService->logDebug(sprintf("Inco %s %s", $summary->incoterm, $summary->incotermPlace));

            $summary->clearanceRequired |= !empty($customsObj->customs_clerance_required);
            foreach ($customsObj->packages as $pck) {
                $priceUnit           = +$pck->price_unit ?? 1; // price refers to x units, e.g. price for 1000 bolts
                $summary->totalValue += $pck->quantity / $priceUnit * $pck->net_price;
                if ($pck->currency_iso)
                    $summary->currency = $pck->currency_iso; // todo: this will only work if all have same currency. Confirm!
            }
            $summary->totalValue = round($summary->totalValue, 2);
            $this->loggerService->logDebug("CustomsSummary created: " . json_encode($summary));
        }

//TODO: Do we need to validate that packaging count on customs tab equals packaging count on packages tab?
        /*            $packagingCount = count($tpoObj->packaging); // from the packaging tab, already known to be > 0
                    $packagesCount  = isset($tpoObj->customs) && is_array($tpoObj->customs->packages) ? count($tpoObj->customs->packages) : 0; // from the customs tab
                    if ($packagingCount != $packagesCount) {
                        $this->strictError(sprintf("Customs input malformed. Need amount of customs positions (%d) to equal packaging positions (%d)",
                            $packagesCount, $packagingCount));
                    }*/
        $summary->clearanceRequired = (bool) $summary->clearanceRequired; // ensure bool and not int. NOTE typecast is unfortunately NOT redundant here
        return $summary;
    }

    /**
     *
     * @throws Exception if DHL credentials missing
     */
    private function getHttpHeaders(): array {
        return [
            "Accept" => "application/json",
            "Content-Type" => "application/json",
            "Authorization" => "Basic " . $this->getAuthString()
        ];
    }

    /**
     * @throws Exception if DHL credentials missing
     */
    private function getAuthString(): string {
        if (!isset($_SERVER['DHL_USR']) || !isset($_SERVER['DHL_PW'])) {
            $this->loggerService->logError("Missing credentials for DHL");
            throw new Exception("Missing credentials for DHL");
        }
        return base64_encode($_SERVER['DHL_USR'] . ":" . $_SERVER['DHL_PW']);
    }


    /**
     * echoes the response to the DHL "POST shipments" request
     * To keep the output readable, the Base64 code is stripped off before echoing
     * @param $responseJson
     */
    public function echoDhlResponse($responseJson): void {
        $retObj = json_decode($responseJson);
        if (isset($retObj->documents)) {
            foreach ($retObj->documents as $idx => $doc) // // remove the large B64 PDF data before echoing
                $retObj->documents[$idx]->content = "---- j3 removed before echoing ----";
        }
        echo "\n\n" . json_encode($retObj, JSON_PRETTY_PRINT) . "\n\n";
    }

    function getB64Label(string $responseJson): ?string {
        $respObj = json_decode($responseJson);
        if (property_exists($respObj, "documents") && is_array($respObj->documents) && count($respObj->documents) > 0) {
            foreach ($respObj->documents as $doc)
                if (property_exists($doc, "typeCode") && $doc->typeCode === "label")
                    if (property_exists($doc, "imageFormat") && $doc->imageFormat === "PDF" && property_exists($doc, "content"))
                        return $doc->content;
        }
        return null;
    }

    function getB64Invoice(string $responseJson): ?string {
        $respObj = json_decode($responseJson);
        if (property_exists($respObj, "documents") && is_array($respObj->documents) && count($respObj->documents) > 0) {
            foreach ($respObj->documents as $doc)
                if (property_exists($doc, "typeCode") && $doc->typeCode === "invoice")
                    if (property_exists($doc, "imageFormat") && $doc->imageFormat === "PDF" && property_exists($doc, "content"))
                        return $doc->content;
        }
        return null;
    }

    function getShipmentDetails(string $responseJson): ?string {
        $respObj = json_decode($responseJson);
        if (property_exists($respObj, "documents") && is_array($respObj->documents) && count($respObj->documents) > 0) {
            foreach ($respObj->documents as $doc)
                if (property_exists($doc, "typeCode") && $doc->typeCode === "invoice")
                    if (property_exists($doc, "imageFormat") && $doc->imageFormat === "PDF" && property_exists($doc, "content"))
                        return $doc->content;
        }
        return null;
    }

    /**
     * extracts the Base64-encoded pdf files from the response to the DHL "POST shipments" request
     * this is typically the labels (all in one file) and in case of paperless Trade, the DHL-rendered invoice
     * @param string $responseJson
     */
    public function writeAllPdfs(string $responseJson): void {
        $respObj = json_decode($responseJson);
        if (property_exists($respObj, "shipmentTrackingNumber"))
            $this->loggerService->logDebug(sprintf("Shipment Requested, tracking Number: %s", $respObj->shipmentTrackingNumber));
        if (property_exists($respObj, "documents") && is_array($respObj->documents) && count($respObj->documents) > 0) {
            foreach ($respObj->documents as $doc)
                if (property_exists($doc, "imageFormat") && property_exists($doc, "content") && $doc->imageFormat === "PDF")
                    $this->writePdfFile($respObj->shipmentTrackingNumber . "_" . $doc->typeCode . ".pdf", $doc->content); // labels and possibly invoice
        }
    }

    /**
     * write Base64 to a file in the var directory
     * @param string $fileName the filename will be prepended by a time stamp Ymd_Hi_
     * @param string $pdfContentB64 Base64 file content
     * @return string|null the filename or null in case of fileCreation trouble
     */
    function writePdfFile(string $fileName, string $pdfContentB64): ?string {
        $fileName           = date("Ymd_Hi_") . $fileName;
        $pdfDecoded         = base64_decode($pdfContentB64);
        $dirName            = ToolsService::upDir(2) . DIRECTORY_SEPARATOR . "var";
        $fileCreationResult = ToolsService::createFile($pdfDecoded, $dirName . DIRECTORY_SEPARATOR . "$fileName");
        if (!$fileCreationResult)
            $this->loggerService->logError("Error writing Pdf file");
        else
            $this->loggerService->logDebug("SUCCESS: PDF file created: " . $fileName);

        return $fileCreationResult ? $fileName : null;
    }
}
