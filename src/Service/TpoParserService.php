<?php
/**
 * Created by PhpStorm.
 * User: Jens
 * Date: 3.03.2020
 * Time: 11:51
 */

namespace App\Service;

use Exception;
use SimpleXMLElement;
use stdClass;

class TpoParserService
{
    private $strictMode; // if true, throws exception immediately when data is missing. Unless TPO_STRICT_MODE explicitly set false in ENV, we will be strict
    const REMOVE_BASE64_CODE = true; // not needed for DHL anyway, and makes the arrays a lot easier to handle, e.g. when debugging

    /** @var LoggerService */
    private $logger;

    public function __construct(LoggerService $logger) {
        $this->logger     = $logger;
        $this->strictMode = !(isset($_SERVER['TPO_STRICT_MODE']) && $_SERVER['TPO_STRICT_MODE'] == 'false'); // unless explicitly set false, we will be strict
    }

    /**
     * @param string $xmlString
     * @return object|null the XML converted to a nice and clean Object
     * @throws Exception a lot of different exceptions thrown, based on input data validity etc
     */
    function parseXmlString(string $xmlString) {
        $xmlInput = simplexml_load_string($xmlString, "SimpleXMLElement", LIBXML_NOCDATA); // CDATA will also be parsed
        if (!$xmlInput)
            throw new Exception("!! ERROR !! couldn't parse xml string");
        return $this->parseXmlInput($xmlInput);
    }

    /**
     * @param string $xmlFilename
     * @return object|null the XML converted to a nice and clean Object
     * @throws Exception a lot of different exceptions thrown, based on input data validity etc
     */
    function parseXmlFile(string $xmlFilename) {
        $xmlInput = simplexml_load_file($xmlFilename, "SimpleXMLElement", LIBXML_NOCDATA); // CDATA will also be parsed
        if (!$xmlInput)
            throw new Exception("!! ERROR !! couldn't parse xml file");
        return $this->parseXmlInput($xmlInput);
    }

    /**
     * @param SimpleXMLElement $xmlInput this data needs to contain all data needed for interfacing to DHL.
     * It is the from the same file, which is used for generating the PDF Transport Order using the BirtWorker
     * @return object|null the XML converted to a nice and clean Object
     * @throws Exception a lot of different exceptions thrown, based on input data validity etc
     */
    function parseXmlInput(SimpleXMLElement $xmlInput) {
//        echo json_encode($xmlInput, JSON_PRETTY_PRINT);
        return $this->createObjWithNiceArrays($xmlInput); // gets rid of redundant hierarchy levels, and ensures that arrays with just 1 element are actually stored as arrays
    }

    function writeParsedDataAsJson($obj): void {
        $dirName            = ToolsService::upDir(2) . DIRECTORY_SEPARATOR . "var";
        $fileCreationResult = ToolsService::createFile(json_encode($obj, JSON_PRETTY_PRINT + JSON_UNESCAPED_UNICODE), $dirName . DIRECTORY_SEPARATOR . "parsedXml.json");
        if (!$fileCreationResult)
            $this->logger->logError("Error writing Parsed XML JSON file");
    }

    /**
     * This recursively called method converts the SimpleXmlElement to a nice clean object and
     * removes the extra hierarchy layer involved with the arrays in the XML transport-orders, invoices etc.,
     * i.e. we will have FinanceDocumentCost[] instead of FinanceDocumentCost.FinanceDocumentCost[]
     * This method also ensures such arrays with just 1 element are still stored as arrays.
     * @param SimpleXMLElement $xml
     * @param string[] $pathSoFar a helper variable used for debugging-echos and
     * for checking if parent and child share the name
     * @return object|array|stdClass normally returns an object, only exception is
     * arrays which are stored as e.g. WayPoint->WayPoint
     */
    function createObjWithNiceArrays(SimpleXMLElement $xml, $pathSoFar = []) {
        $retArray = [];
        $retObj   = new stdClass();
        foreach ($xml->children() as $child) {
            if ($child->children()->count() < 1) {
                $childName = $child->getName();
                if ($childName === "country_flag")
                    $retObj->{$childName} = "..."; // remove b64 string to keep logs short and readable
                else
                    $retObj->{$childName} = (string) $child;
//                echo implode("->", $pathSoFar) . "->" . $child->getName() . " VAL = " . $child[0] . "\n";
            } else {
                $parentAndChildSameName = (end($pathSoFar) == $child->getName());
                $pathSoFar[]            = $child->getName(); // push
                if ($parentAndChildSameName) { // if same name,, we are dealing with an array, and we collapse the 2 levels into a simple array
                    $retArray[] = $this->createObjWithNiceArrays($child, $pathSoFar);
                } else {
                    $retObj->{$child->getName()} = $this->createObjWithNiceArrays($child, $pathSoFar);
                }
                array_pop($pathSoFar);
            }
        }
        if (count($retArray)) {
            if (!empty(get_object_vars($retObj)))
                $this->strictError(sprintf("Saw 2 hierarchy levels with same name, and assumed it was a simple array - however there seems to be unexpected data also %s->%s ", $child->getName(), $child->getName()));
            return $retArray;
        } else
            return $retObj;
    }

    /**
     * Throws exception if in STRICT MODE, otherwise just logs the event preceded by the word: "UNSTRICT:"
     * @param string $errorStr
     * @throws Exception
     */
    private function strictError(string $errorStr) {
        if ($this->strictMode)
            throw new Exception($errorStr);
        else
            $this->logger->logNotice("UNSTRICT: " . $errorStr); // would have thrown exception if we had been in STRICT Mode
    }

}
