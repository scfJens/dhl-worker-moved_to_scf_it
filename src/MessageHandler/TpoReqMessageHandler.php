<?php
/**
 * Created by PhpStorm.
 * User: jens.jakobsen
 * Date: 2019-04-30
 * Time: 10:41
 */

namespace App\MessageHandler;

use App\Message\TpoReqMessage;
use App\Service\LoggerService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Messenger\MessageBusInterface;

class TpoReqMessageHandler implements MessageHandlerInterface
{

    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var LoggerService */
    private $loggerService;

    /** @var MessageBusInterface */
    private $ipcBackChannel;


    public function __construct(EntityManagerInterface $entityManager, LoggerService $logger, MessageBusInterface $bus)
    {
        $this->entityManager = $entityManager;
        $this->loggerService = $logger;
        $this->ipcBackChannel = $bus;
    }

    public function __invoke(TpoReqMessage $tpoMsg)
    {
        $this->loggerService->logInfo("TpoReqMessageHandler called, designFileName: " . $tpoMsg->designFileName);

    }
}
