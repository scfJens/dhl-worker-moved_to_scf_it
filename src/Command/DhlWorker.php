<?php
/**
 * Created by PhpStorm.
 * User: Jens
 * Date: 02.03.2020
 * Time: 12:38
 */

namespace App\Command;

use App\Message\TpoReqMessage;
use App\Model\SqsMessage;
use App\Service\DhlService;
use App\Service\LoggerService;
use App\Service\SqsService;
use App\Service\ToolsService;
use App\Service\TpoParserService;
use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;


class DhlWorker extends Command
{
    const SHOW_STACK_TRACE       = false;
    const SHOW_STACK_TRACE_DEPTH = 3;

    /** @var DhlService */
    private $dhlService;

    /** @var LoggerService */
    private $loggerService;

    /** @var TpoParserService */
    private $tpoService;

    /** @var SqsService */
    private $sqsService; // used for receiving requests and sending responses to TMS. Also used for sending requests and receiving responses from BirtWorker

    public function __construct(LoggerService $logger, TpoParserService $tpoService, DhlService $dhl, SqsService $sqs) {
        parent::__construct(null);
        $this->loggerService = $logger;
        $this->tpoService = $tpoService;
        $this->dhlService = $dhl;

        $this->sqsService = $sqs;
    }

    protected function configure() {
        $this->setName('app:dhl-worker')->setDescription("Listening for DHL requests, then 1: creating PDF, 
        2: parsing msg, 3: sending .... 
        and finally 5: returning the PDF to requester along with a status code");
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output) {
        $this->loggerService->logDebug("DHL Worker starting up");
        $startTime               = time();

        while (true) {
            $this->restartIfNeeded($startTime);

            $result = $this->sqsService->listenForTpoRequests();
            if (!$result) {
                $this->loggerService->logError("DHL Request listener returned null, trying to continue");
            } else {
                $msgCount = empty($result->get('Messages')) ? 0 : count($result->get("Messages"));
                $this->loggerService->logDebug("received " . $msgCount . " message" . ($msgCount !== 1 ? "s" : ""));
                if ($msgCount)
                    foreach ($result->get("Messages") as $msg) {
                        /* @var $request SqsMessage */
                        $request = null;
                        try {
                            $request = new SqsMessage($msg);// initiate helperObject, converting the anonymous array to an object
                        } catch (Exception $e) {
                            $this->loggerService->logError("Error couldn't decode the request, thus cannot send response, trying to continue with next message");
                        }
                        if ($request) { // will be zero if we couldn't parse the request from TMS
                            try {
                                $this->handleTmsRequest($request);
                            } catch (Exception $e) {
                                $errStr = "DHL Worker throwing exception: " . $e->getMessage();
//                                ToolsService::echoStringAsHexDump($e->getMessage());
                                $this->loggerService->logError($errStr);
                                $correlationId = $request->getMessageId();
                                $this->sendErrorResponse($errStr, $correlationId);
                                $this->sqsService->deleteTpoRequest($request);

                            }
                        }
                    }
            }
        }
        return 0;
    }

    private function restartIfNeeded(int $startTime) {
        if (time() - $startTime < 7200)
            return; // restart not possible if we were started less than two hours ago
        $restartHrs = [];
        if (isset($_SERVER['RESTART_HR_1']))
            $restartHrs[] = $_SERVER['RESTART_HR_1'];
        if (isset($_SERVER['RESTART_HR_2']))
            $restartHrs[] = $_SERVER['RESTART_HR_2'];
        if (in_array(getdate()['hours'], $restartHrs, false)) {
            $this->loggerService->logDebug("Time is up for RESTARTING ...");
            die("24 hrs passed, now restarting the DHL Worker");
        }
    }

    protected function listDir(string $path) {
        $thisDir = scandir($path);
        if ($thisDir)
            foreach ($thisDir as $fil)
                $this->loggerService->logDebug(sprintf("found in %s: %s", $path, $fil));
        else
            $this->loggerService->logDebug(sprintf("didn't find dir: %s", $path));
    }

    /**
     * Takes the request from TMS and goes through the following steps:
     * 1: forward request without modification to the BirtWorker (might be a link to an S3 bucket in case of a large request)
     * 2: get tpo pdf as response from BirtWorker
     * 3: Test if the message already contains its payload (normal case) or if the payload was stored in an S3 bucket (in case msg was >256kB)
     * 4: Extract payload of the original TMS request from an S3 bucket if needed
     * 5: parse the XML data (same as used by the BirtWorker)
     * 6: DHL API: possibly get DHL rates, or ...
     * 7: DHL API: ... create Shipment
     * 8: send the tpo pdf and the DHL content back to TMS as a response to the original request (correlationId set = original Request MsgID)
     *  Will only return the pdf if all went well, so getting the pdf means: All is good, including DHL interaction
     *  If an error happens we throw an exception and the caller needs to send an error response to the Requestor ($this->sendErrorReply($errStr, $correlationId);)
     *  ... and possibly also delete the request from the queue - otherwise it will be processed a 2nd time on the next queue connection
     * @param SqsMessage $request the original request from TMS (fits Message/TpoRequestMessage.php)
     * @throws Exception
     */
    protected function handleTmsRequest(SqsMessage $request) {
        $rawBody = $request->getBody(); // can be real payload, or just an s3 bucket link
        $tpoReq = (new TpoReqMessage()); // the payload, i.e. the request, possibly get from S3 bucket
        $tpoReq->init($this->sqsService->getPayload($request));
        $attr = $request->getMessageAttributes();
        $correlationId = $request->getMessageId();  // we will use this at the end, when responding back to TMS
        $this->loggerService->logDebug("TMS Request Received, SQS Msg ID: " . $correlationId);
        if (isset($_SERVER['LOG_XML_INPUT']) && $_SERVER['LOG_XML_INPUT'] === "true") { // log only if explicitly set true
            $this->loggerService->logDebug("TMS Request Received, XML content: " . $tpoReq->xmlData);
        }
        if (!$tpoReq->rateRequestOnly) {
// get pdf from birtworker, unless we just want to get the DHL rates
            $extendedPayloadAttr = array_key_exists("ExtendedPayloadSize", $attr) ? $attr["ExtendedPayloadSize"] : null;
            $pdfResponseJson = $this->sqsService->doRpcCall($rawBody, $extendedPayloadAttr); // fwd the raw body as received, might be an S3 Pointer
            $pdfObj = json_decode($pdfResponseJson);
//check if pdf creation success before DHL stuff
            if (isset($pdfObj->error)) {
                $this->logAndThrowException("ERROR!!! BirtWorker/DhlWorker error: " . $pdfObj->error);
            }
            $this->loggerService->logDebug("PDF creation went well");
        }
// parse the tpo message before using the DHL API
        // so far we haven't touched the original TMS request (was forwarded as is to the birtWorker for pdf creation).
        try {
            $tpoXmlObject = $this->tpoService->parseXmlString($tpoReq->xmlData);
            $this->loggerService->logDebug(sprintf("TPO Message just parsed (" . strlen($tpoReq->xmlData) . " bytes):\n"));
        } catch (Exception $e) {
            if (self::SHOW_STACK_TRACE)
                ToolsService::showStackTrace($this->loggerService, $e, self::SHOW_STACK_TRACE_DEPTH);
            $this->logAndThrowException("ERROR: Failed parsing TPO Message: " . $e->getMessage());
        }
        if (isset($_SERVER['LOG_TPO_CONTENT']) && $_SERVER['LOG_TPO_CONTENT'] === "true") { // log only if explicitly set true
            echo "Data based on received xml data:\n";
            echo "$tpoXmlObject\n\n";
        }

//        die();

// now call DHL API
        if ($tpoReq->rateRequestOnly) { // get DHL Rates
            $response = $this->getDhlRates($tpoXmlObject);
            $this->sqsService->sendResponse(json_encode(
                ["dhlDetails" => $response]
            ), $correlationId);
        } else { // generate DHL Shipment
//        $pdfObj->dhlLabel = $this->dhlService->getB64Label($response);
//        $pdfObj->dhlInvoice = $this->dhlService->getB64Invoice($response); // not needed since part of response below
            $response = $this->generateDhlShipment($tpoXmlObject, $tpoReq->productCode);
            $pdfObj->dhlDetails = json_decode($response); // include the full DHL response in the response message
            $this->sqsService->sendResponse(json_encode($pdfObj), $correlationId);
        }
        $this->sqsService->deleteTpoRequest($request);
    }

    /**
     * @param $tpoObject
     * @return string
     * @throws TransportExceptionInterface
     * @throws Exception
     */
    private function generateDhlShipment($tpoObject, $productCode): string {
        try {
            $shipReqJson = $this->dhlService->generateShipmentReqBody($tpoObject, $productCode);
            $response = $this->dhlService->postShipmentRequest($shipReqJson);
            if (!ToolsService::isJson($response)) {
                throw new Exception("++++++ DHL SHIPMENT FAILED: +++++++++ Fail details: $response");
            }
            $this->dhlService->writeAllPdfs($response); // todo: remove before going live
//                    if (property_exists($respObj->documents[0], "imageFormat") && property_exists($respObj->documents[0], "content") && $respObj->documents[0]->imageFormat === "PDF")
//                        $this->writePdfFile($respObj->shipmentTrackingNumber . ".pdf", $respObj->documents[0]->content);
        } catch (Exception $e) {
            $this->logAndThrowException("ERROR generating DHL Shipment Request: " . $e->getMessage());
        }
        return $response;
    }

    /**
     * @param $tpoObject
     * @return string
     * @throws TransportExceptionInterface
     * @throws Exception
     */
    private function getDhlRates($tpoObject): string {
        try {
            $shipReqJson = $this->dhlService->generateRatesReqBody($tpoObject);
            $response = $this->dhlService->postRateRequest($shipReqJson);
            if (!ToolsService::isJson($response)) {
                throw new Exception("++++++ DHL RESPONSE WAS NOT JSON AS EXPECTED: +++++++++$response");
            }
        } catch (Exception $e) {
            $this->logAndThrowException("ERROR generating DHL Rating Request: " . $e->getMessage());
        }
        return $response;
    }

    /**
     * Logs error and throws exception
     * @param string $errMsg
     * @throws Exception
     */
    private function logAndThrowException(string $errMsg) {
        $this->loggerService->logError($errMsg);
        throw new Exception($errMsg);
    }

    /**
     * If something goes wrong, we will return an error response instead of the pdf to the requestor.
     * @param string $errorMessage
     * @param string $correlationId
     */
    private function sendErrorResponse(string $errorMessage, string $correlationId) {
        $this->loggerService->logError("DHL Worker problem: " . $errorMessage);
        $returnJson = json_encode(["error" => $errorMessage]);
        $this->sqsService->sendResponse($returnJson, $correlationId);
    }

}
