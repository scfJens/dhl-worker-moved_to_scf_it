<?php
/**
 * Created by PhpStorm.
 * User: Jens
 * Date: 08.02.2019
 * Time: 12:38
 */

namespace App\Command;

use App\Service\DhlService;
use App\Service\LoggerService;
use App\Service\ToolsService;
use App\Service\TpoParserService;
use stdClass;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class GeneralTester extends Command
{
    const TEST_QUEUE_NAME = "j3ExtendedSqsTestQueue";
    const S3_BUCKET_NAME  = "j3testbucket25sep2020";
    const MAX_RETRIES     = 5;

    /** @var LoggerService */
    private $loggerService;

    /** @var DhlService */
    private $dhlService;

    /** @var DhlWorker */
    private $dhlWorker;
    /** @var TpoParserService */
    private $tpoParserService;


    public function __construct(LoggerService $logger, DhlService $dhl, DhlWorker $worker, TpoParserService $tpoParserService) {
        parent::__construct(null);
        $this->loggerService    = $logger;
        $this->dhlService       = $dhl;
        $this->dhlWorker        = $worker;
        $this->tpoParserService = $tpoParserService;
    }

    protected function configure() {
        $this->setName('app:test')->setDescription("Testing various functions");
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $outputl
     * @return int
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     */
    protected function execute(InputInterface $input, OutputInterface $outputl) {
        $this->testShipReq31Mar2022();
        die(0);


//        $xmlFilePath = "../assets/saYem - Mon Sep 13 2021 10_40_34 GMT+0200 (Central European Summer Time)-customs.xml";
//        $xmlFilePath = "../assets/saYem - Thu Sep 16 2021 10_56_03 GMT+0200 (Central European Summer Time)-moreDHLFields.xml";
//        $xmlFilePath = "../assets/saYem - Mon Sep 20 2021 11_55_15 GMT+0200 (Central European Summer Time).xml";
//        $xmlFilePath = "../assets/saYem - Mon Sep 20 2021 12_52_28 GMT+0200 (Central European Summer Time).xml";
//        $xmlFilePath = "../assets/saYem - Mon Sep 20 2021 14_48_34 GMT+0200 (Central European Summer Time).xml";
        $xmlFilePath = "../assets/saYem - Mon Sep 20 2021 16_13_40 GMT+0200 (Central European Summer Time).xml";
//        $xmlFilePath = "../assets/165882_modified_for_bentley.xml";
//        $xmlFilePath = "../assets/165882_201910041128_mann_hummel_gmbh_bgm_express_logistik_gmbh.xml";
        $tpoObjct = $this->tpoParserService->parseXmlFile($xmlFilePath);
//        echo "\n\n++++++ AND HERE THE OBJECT: +++++++++\n\n";
//        echo json_encode($tpoObjct, JSON_PRETTY_PRINT);
//        die();
        try {
            $response = $this->doRatingRequest($tpoObjct);
            $this->dhlService->echoDhlResponse($response);
        } catch (\Exception $e) {
            $this->loggerService->logDebug("ERROR doRatingRequest failed: " . $e->getMessage());
        }
        try {
            $response = $this->doShipmentRequest($tpoObjct);
            $this->dhlService->echoDhlResponse($response);
        } catch (\Exception $e) {
            $this->loggerService->logDebug("ERROR doRatingRequest failed: " . $e->getMessage());
        }
        return 0;
    }

    function testShipReq31Mar2022() {
        $xmlFilePath = "../assets/Marcus12Apr2022/ShipmReq18May2022.xml";
        $tpoObj      = $this->tpoParserService->parseXmlFile($xmlFilePath);

    /*    if (!$tpoObj->waypoint[0]->mobile && property_exists($tpoObj->waypoint[0], "position"))
            $tpoObj->waypoint[0]->mobile = $tpoObj->waypoint[0]->position;
      if (!$tpoObj->waypoint[1]->mobile && property_exists($tpoObj->waypoint[1], "position"))
            $tpoObj->waypoint[1]->mobile = $tpoObj->waypoint[1]->position;

        echo json_encode($tpoObj, JSON_PRETTY_PRINT);



        return;
*/


        $shipReqJson = $this->dhlService->generateShipmentReqBody($tpoObj, "P");
        echo "$shipReqJson\n";


        die(4);

        $response = $this->dhlService->postShipmentRequest($shipReqJson);

        if (ToolsService::isJson($response)) {
            $this->dhlService->echoDhlResponse($response);
            $this->dhlService->writeAllPdfs($response);
//                    if (property_exists($respObj->documents[0], "imageFormat") && property_exists($respObj->documents[0], "content") && $respObj->documents[0]->imageFormat === "PDF")
//                        $this->writePdfFile($respObj->shipmentTrackingNumber . ".pdf", $respObj->documents[0]->content);
        } else {

            echo "\n\n++++++ DHL RESPONSE WAS NOT JSON AS EXPECTED: +++++++++\n$response\n";
        }
    }


    /**
     * @param $tpoObject
     * @throws TransportExceptionInterface
     */
    private function doShipmentRequest($tpoObject): string {
        try {
            $shipReqJson = $this->dhlService->generateShipmentReqBody($tpoObject, "P");
            echo "$shipReqJson\n";


            die(3);


            $response = $this->dhlService->postShipmentRequest($shipReqJson);

            if (ToolsService::isJson($response)) {
                $this->dhlService->echoDhlResponse($response);
                $this->dhlService->writeAllPdfs($response);
//                    if (property_exists($respObj->documents[0], "imageFormat") && property_exists($respObj->documents[0], "content") && $respObj->documents[0]->imageFormat === "PDF")
//                        $this->writePdfFile($respObj->shipmentTrackingNumber . ".pdf", $respObj->documents[0]->content);
            } else {

                echo "\n\n++++++ DHL RESPONSE WAS NOT JSON AS EXPECTED: +++++++++\n$response\n";
            }
            return $response;

        } catch (\Exception $e) {
            $this->loggerService->logDebug("ERROR generating DHL Shipment Request: " . $e->getMessage());
            return "ERROR generating DHL Shipment Request: " . $e->getMessage();
        }

    }

    private function doRatingRequest($tpoObject): string {
        try {
            $shipReqJson = $this->dhlService->generateRatesReqBody($tpoObject);
            echo "$shipReqJson\n";

            die(2);

            $response = $this->dhlService->postRateRequest($shipReqJson);

            if (!ToolsService::isJson($response)) {
                echo "\n\n++++++ DHL RESPONSE WAS NOT JSON AS EXPECTED: +++++++++\n$response\n";
            }
            return $response;


        } catch (\Exception $e) {
            $this->loggerService->logDebug("ERROR generating DHL Rating Request: " . $e->getMessage());
            return "ERROR generating DHL Rating Request: " . $e->getMessage();
        }
    }


    function testXml() {
        $testXml     = <<<XML
        <root>
          <comment>this comment is part of my payload and should be parsed</comment>
           <comment>this comment is also part of my payload and should also be parsed</comment>
         
                    <node>
          </node>
          <!-- this comment should not be parsed-->
        </root>
XML;
        $xmlDataTest = simplexml_load_string($testXml);
        var_dump($xmlDataTest);
//        var_dump($xmlDataTest->comment);
        $this->loggerService->logDebug("comment count: " . $xmlDataTest->comment->count());
        $this->loggerService->logDebug("comment: " . $xmlDataTest->comment);
        $this->loggerService->logDebug("comment: " . $xmlDataTest->comment[0]);
        $this->loggerService->logDebug("comment: " . $xmlDataTest->comment[1]);

        $json = json_encode($xmlDataTest, JSON_PRETTY_PRINT);
        echo "\n\n" . $json;

//<!--          <comment>this comment again is also part of my payload and should be parsed</comment>-->

    }

    function traverseXml(\SimpleXMLElement $xml) {
        foreach ($xml->children() as $second_gen) {
            echo ' Root->' . $second_gen->getName();

            foreach ($second_gen->children() as $third_gen) {
                echo '->' . $third_gen->getName() . ';';

                foreach ($third_gen->children() as $fourth_gen) {
                    echo "\n------------>" . $fourth_gen->getName();
                    foreach ($fourth_gen->children() as $fifth_gen) {
                        echo "\n------------------------------------>" . $fifth_gen->getName();
                    }

                }
            }
            echo "\n";
        }
    }


    function traverseXml2(\SimpleXMLElement $xml, array $pathSoFar = ["root"]): array {
//        var_dump($xml);

        $retArray = [];


        foreach ($xml->children() as $secondGen) {
            if ($secondGen->children()->count() < 1) {
                echo implode("->", $pathSoFar) . "->" . $secondGen->getName();
                echo ' VAL = ' . $secondGen[0] . "\n";
//                var_dump($secondGen);
//                $retArray[$secondGen->getName()] = (string) $secondGen[0];
                $retArray = $this->insertValDirectlyOrInArray($retArray, $secondGen->getName(), (string) $secondGen[0]);
            } else {
                $pathSoFar[] = $secondGen->getName();
                $retArray    = $this->insertValDirectlyOrInArray($retArray, $secondGen->getName(), $this->traverseXml2($secondGen, $pathSoFar));
                array_pop($pathSoFar);
            }
        }
        return $retArray;
    }

    /**
     * inserts a value into an array. If the key already has a val, the val is converted in to an array containing that val, and the new val is then pushed onto that array
     * @param array $retArray
     * @param string $key
     * @param $val
     * @return array
     */
    function insertValDirectlyOrInArray(array $retArray, string $key, $val): array {
        if (array_key_exists($key, $retArray)) { // if property already exists, convert to array before adding the new value
            if (!is_array($retArray[$key]))
                $retArray[$key] = [$retArray[$key]];
            $retArray[$key][] = $val;
        } else {
            $retArray[$key] = $val;
        }
        return $retArray;
    }

    /**
     * This recursively called method removes the extra hierarchy layer involved with the arrays in the XML transport-orders etc.,
     * i.e. we will have FinanceDocumentCost[] instead of FinanceDocumentCost.FinanceDocumentCost[]
     * This method also ensures such arrays with just 1 element are still stored as arrays.
     * @param \SimpleXMLElement $xml
     * @param array|string[] $pathSoFar
     * @return object|array|stdClass normally returns an object, only exception is
     * arrays stored as e.g. WayPoint.WayPoint
     */
    function ensureNiceArrays2(\SimpleXMLElement $xml, array $pathSoFar = []) {
        $retArray = [];
        $retObj   = new stdClass();
        foreach ($xml->children() as $child) {
            if ($child->children()->count() < 1) {
                $retObj->{$child->getName()} = (string) $child[0];
//                echo implode("->", $pathSoFar) . "->" . $child->getName();
//                echo ' VAL = ' . $child[0] . "\n";
            } else {
                $parentAndChildSameName = (end($pathSoFar) == $child->getName());
//                if ($nameFoundTwice)    $this->loggerService->logInfo("name found twice: " . end($pathSoFar));
                $pathSoFar[] = $child->getName();
                if ($parentAndChildSameName) { // if property already exists, we are dealing with an array, and collypes the 2 levels
                    $retArray[] = $this->ensureNiceArrays2($child, $pathSoFar);
                } else {
                    $retObj->{$child->getName()} = $this->ensureNiceArrays2($child, $pathSoFar);
                }
                array_pop($pathSoFar);
            }
        }
        if (count($retArray)) {
            if (!empty(get_object_vars($retObj)))
                $this->loggerService->logError(sprintf("saw 2 hierarchy levels with same name, and assumed it was a simple array - however there seems to be unexpected data also %s->%s ", $child->getName(), $child->getName()));
            return $retArray;
        } else
            return $retObj;
    }

    function writeXmlToFile(string $xmlIn) {
        $xmlIn              = preg_replace('/\R/u', "\n", $xmlIn); // any Unicode newline sequence
        $fileName           = date("Ymd_His_") . "XML_input.xml";
        $dirName            = ToolsService::upDir(2) . DIRECTORY_SEPARATOR . "var";
        $fileCreationResult = ToolsService::createFile($xmlIn, $dirName . DIRECTORY_SEPARATOR . "$fileName");
        if (!$fileCreationResult)
            $this->loggerService->logError("Error writing XML file");
        else
            $this->loggerService->logDebug("SUCCESS: XML file created: " . $fileName);

    }


}
