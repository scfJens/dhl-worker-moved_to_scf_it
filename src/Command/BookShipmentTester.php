<?php
/**
 * Created by PhpStorm.
 * User: Jens
 * Date: 08.02.2019
 * Time: 12:38
 */

namespace App\Command;

use App\Service\DhlService;
use App\Service\LoggerService;
use App\Service\ToolsService;
use App\Service\TpoParserService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class BookShipmentTester extends Command
{

    /** @var LoggerService */
    private $loggerService;

    /** @var DhlService */
    private $dhlService;

    /** @var TpoParserService */
    private $tpoParserService;


    public function __construct(LoggerService $logger, DhlService $dhl, TpoParserService $tpoParserService) {
        parent::__construct(null);
        $this->loggerService    = $logger;
        $this->dhlService       = $dhl;
        $this->tpoParserService = $tpoParserService;
    }

    protected function configure() {
        $this->setName('app:shipmTest')->setDescription("Get DHL Shipment - based on TPO.xml");
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $outputl
     * @return int
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     */
    protected function execute(InputInterface $input, OutputInterface $outputl) {
//        $xmlFilePath = "../assets/BarbaraApproval/30May2023-3rd-approval-customs/ShipmentBooking.xml";
        $xmlFilePath = "../assets/May2024/RaulShipmentSuccess16May2024.xml";
        $tpoObjct = $this->tpoParserService->parseXmlFile($xmlFilePath);

        echo json_encode($tpoObjct, JSON_PRETTY_PRINT);

        echo "\n+++++++++++++\n+++++++++++++\n+++++++++++++\n+++++++++++++\n+++++++++++++\n";

        try {
            $response = $this->doShipmentRequest($tpoObjct);
            $this->dhlService->echoDhlResponse($response);
        } catch (\Exception $e) {
            $this->loggerService->logDebug("ERROR doRatingRequest failed: " . $e->getMessage());
        }
        return 0;
    }

    /**
     * @param $tpoObject
     * @throws TransportExceptionInterface
     */
    private function doShipmentRequest($tpoObject): string {
        try {
            $shipReqJson = $this->dhlService->generateShipmentReqBody($tpoObject, "P"); // "P" = EXPRESS WORLDWIDE NONDOC
            echo "$shipReqJson\n";

          die(3);

            $response = $this->dhlService->postShipmentRequest($shipReqJson);

            if (ToolsService::isJson($response)) {
                $this->dhlService->writeAllPdfs($response);
            } else {
                echo "\n\n++++++ DHL RESPONSE WAS NOT JSON AS EXPECTED: +++++++++\n$response\n";
            }
            return $response;

        } catch (\Exception $e) {
            $this->loggerService->logDebug("ERROR generating DHL Shipment Request: " . $e->getMessage());
            return "ERROR generating DHL Shipment Request: " . $e->getMessage();
        }

    }

}
