<?php
/**
 * Created by PhpStorm.
 * User: Jens
 * Date: 08.02.2019
 * Time: 12:38
 */

namespace App\Command;

use App\Service\DhlService;
use App\Service\LoggerService;
use App\Service\ToolsService;
use App\Service\TpoParserService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GetRatesTester extends Command
{
    /** @var LoggerService */
    private $loggerService;

    /** @var DhlService */
    private $dhlService;

    /** @var TpoParserService */
    private $tpoParserService;


    public function __construct(LoggerService $logger, DhlService $dhl, TpoParserService $tpoParserService) {
        parent::__construct();
        $this->loggerService    = $logger;
        $this->dhlService       = $dhl;
        $this->tpoParserService = $tpoParserService;
    }

    protected function configure() {
        $this->setName('app:ratesTest')->setDescription("Get DHL Rates - based on TPO.xml");
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $outputl
     * @return int
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     */
    protected function execute(InputInterface $input, OutputInterface $outputl) {
//        $xmlFilePath = "../assets/BarbaraApproval/30May2023-3rd-approval-customs/RateRequest.xml";
        $xmlFilePath = "../assets/May2024/raulRatingReq3-995.xml";
        $tpoObject = $this->tpoParserService->parseXmlFile($xmlFilePath);
        echo json_encode($tpoObject, JSON_PRETTY_PRINT);

        echo "\n**************\n**************\n**************\n**************\n";
        try {
            $response = $this->doRatingRequest($tpoObject);
            $this->dhlService->echoDhlResponse($response);
        } catch (\Exception $e) {
            $this->loggerService->logDebug("ERROR doRatingRequest failed: " . $e->getMessage());
        }
        return 0;
    }

    private function doRatingRequest($tpoObject): string {
        try {
            $shipReqJson = $this->dhlService->generateRatesReqBody($tpoObject);
            echo "$shipReqJson\n";

//            die(2);

            $response = $this->dhlService->postRateRequest($shipReqJson);

            if (!ToolsService::isJson($response)) {
                echo "\n\n++++++ DHL RESPONSE WAS NOT JSON AS EXPECTED: +++++++++\n$response\n";
            }
            return $response;


        } catch (\Exception $e) {
            $this->loggerService->logDebug("ERROR generating DHL Rating Request: " . $e->getMessage());
            return "ERROR generating DHL Rating Request: " . $e->getMessage();
        }
    }

}
