<?php
/**
 * Created by PhpStorm.
 * User: Jens
 * Date: 08.02.2019
 * Time: 12:38
 */

namespace App\Command;

use App\Service\DhlService;
use App\Service\LoggerService;
use App\Service\ToolsService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class BookShipmentFromJsonTester extends Command
{

    /** @var LoggerService */
    private $loggerService;

    /** @var DhlService */
    private $dhlService;

    public function __construct(LoggerService $logger, DhlService $dhl) {
        parent::__construct(null);
        $this->loggerService    = $logger;
        $this->dhlService       = $dhl;
    }

    protected function configure() {
        $this->setName('app:shipmTestJson')->setDescription("Get DHL Shipment - based on json body. The body was taken from our logs");
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $outputl
     * @return int
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     */
    protected function execute(InputInterface $input, OutputInterface $outputl) {
        $reqBody = file_get_contents("../assets/May2024/RaulShipmentReqFail-Incoterm.json");

        echo "\n+++++++++++++\n+++++++++++++\n+++++++++++++\n+++++++++++++\n+++++++++++++\n";

        try {
            $response = $this->doShipmentRequest($reqBody);
            $this->dhlService->echoDhlResponse($response);
        } catch (\Exception $e) {
            $this->loggerService->logDebug("ERROR doRatingRequest failed: " . $e->getMessage());
        }
        return 0;
    }

    /**
     * @param $jsonBody
     * @throws TransportExceptionInterface
     */
    private function doShipmentRequest($jsonBody): string {
        try {

            $response = $this->dhlService->postShipmentRequest($jsonBody);

            if (ToolsService::isJson($response)) {
                $this->dhlService->writeAllPdfs($response);
            } else {
                echo "\n\n++++++ DHL RESPONSE WAS NOT JSON AS EXPECTED: +++++++++\n$response\n";
            }
            return $response;

        } catch (\Exception $e) {
            $this->loggerService->logDebug("ERROR generating DHL Shipment Request: " . $e->getMessage());
            return "ERROR generating DHL Shipment Request: " . $e->getMessage();
        }

    }

}
