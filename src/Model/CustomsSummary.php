<?php

namespace App\Model;

class CustomsSummary
{
    /** @var bool */
    public $clearanceRequired = false;

    /** @var string */
    public $incoterm;

    /** @var string */
    public $incotermPlace;

    /** @var float */
    public $totalValue = 0;

    /** @var string */
    public $currency;

    /** @var string */
    public $eoriNumber; // consists of a country code (2 letters) followed by an identifier that is unique in the Member State (up to 15 alphanumeric characters)

}
