<?php

namespace App\Message;

/**
 * When requesting a TPO creation and submit to DHL, also a PDF for the transport order will be generated (using the BirtWorker Micro service)
 */
class TpoReqMessage
{
    /** @var string */
    public $transactionId; // will be the first part of the returned pdf filename, so that it is clear where the report belongs
    /** @var string */
    public $xmlData; // This is all what is needed for generating the TPO and DHL ..
    /** @var string */
    public $designFileName; // For BirtWorker, one of the design files in the local resources folder of BirtWorker
    /** @var string[] */
    public $assetFileData; // For BirtWorker, asset files that are text based and thus not encoded
    /** @var string[] */
    public $assetFileNames; // For BirtWorker, the filenames for the text based asset files
    /** @var string[] */
    public $assetFileDataB64; // For BirtWorker, asset files that are binary and thus base64 encoded
    /** @var string[] */
    public $assetFileNamesB64; // For BirtWorker, the filenames for the base64 encoded asset files
    /** @var string */
    public $locale; // eg: es_ES
    /** @var float */
    public $dpi; // e.g. 96.0
    /** @var boolean */
    public $rateRequestOnly = false; // if set, no pdf will be returned, only a json containing the rates
   /** @var string */
    public $productCode; // DHL Product Code
/** @var string */
    public $localProductCode; // DHL local Product Code

    /**
     * @param string $inData a json string
     */
    public function init(string $inData)
    {
        $inObj = json_decode($inData,false);

//        var_dump($inData);
//        echo "\n\n";
        foreach ($inObj as $key => $val) {
            if (property_exists(__CLASS__, $key)) {
                $this->$key = $val;
            }
        }
    }


}
